<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// test
$route['hello'] = 'Hello_controller/hello';

$route['login/company'] = 'Authen_controller/Login';
$route['login/user'] = 'Authen_controller/loginApplicant';
$route['logout'] = 'Authen_controller/Logout';
$route['logout/user'] = 'Authen_controller/logoutApplicant';
$route['getcountannounce'] = 'Authen_controller/CountAnnounce';
$route['getcountannouncefavorite'] = 'Authen_controller/CountAnnounceFavorite';
$route['getannounceconnect'] = 'Authen_controller/getannounceconnect';
$route['getannounce'] = 'Authen_controller/getannounceAll';
$route['getannounce_content'] = 'Authen_controller/getAnnounce_content';
$route['getannounce_search'] = 'Authen_controller/getannounce_search';
$route['getannounce_topview'] = 'Authen_controller/getannounce_topview';
$route['getannounce_topfavorite'] = 'Authen_controller/getannounce_topfavorite';
$route['getannounce_topincome'] = 'Authen_controller/getannounce_topincome';
$route['getannounce_myfavorite'] = 'Authen_controller/getannounce_myfavorite';
$route['getannounce_month'] = 'Authen_controller/getannounce_month';
$route['countView'] = 'Authen_controller/countView';
$route['getAnnounceById'] = 'Authen_controller/getAnnounceById';
$route['getHiringCarousel'] = 'Authen_controller/getHiringCarousel';
$route['auth/createuser'] = 'Authen_controller/UsersCreate';
$route['auth/createcompany'] = 'Authen_controller/CompanyCreate';
$route['auth/chkEmailDuplicate'] = 'Authen_controller/chkEmailDuplicate';
$route['auth/comfirm'] = 'Authen_controller/comfirm';
$route['auth/chkPassCode'] = 'Authen_controller/chkStatus';
$route['auth/Chk_applicant'] = 'Authen_controller/Chk_applicant';
$route['auth/register'] = 'Authen_controller/register';
$route['auth/logUser'] = 'Authen_controller/logUser';

$route['company/detail'] = 'Company_controller/CompanyDetail';
$route['company/AnnounceAdd'] = 'Company_controller/AnnounceAdd';
$route['company/AnnounceEdit'] = 'Company_controller/AnnounceEdit';
$route['company/announce_files'] = 'Company_controller/Announce_files';
$route['company/EditCompany'] = 'Company_controller/EditCompanyDetail';
$route['company/getCompanyDetail'] = 'Company_controller/getCompanyDetail';
$route['company/company_files'] = 'Company_controller/company_files';
$route['company/company_detail_files'] = 'Company_controller/company_detail_files';
$route['company/DashboardCount'] = 'Company_controller/DashboardCount';
$route['company/DashboardTop10view'] = 'Company_controller/DashboardTop10view';
$route['company/DashboardTop10Apply'] = 'Company_controller/DashboardTop10Apply';
$route['company/getannounceByCreate'] = 'Company_controller/getannounceByCreate';
$route['company/updateStatusTrash'] = 'Company_controller/updateStatusTrash';
$route['company/getApplicantByAnnounce'] = 'Company_controller/getApplicantByAnnounce';
$route['company/getViewByAnnounce'] = 'Company_controller/getViewByAnnounce';
$route['applicant/getProfileID'] = 'Company_controller/getProfileID';
$route['applicant/GetActivityByUid'] = 'Company_controller/GetActivityByUid';
$route['applicant/CountActivityByUid'] = 'Company_controller/CountActivityByUid';


$route['users/InviteUsers'] = 'Users_controller/InviteUsers';
$route['users/getprofile'] = 'Users_controller/getProfile';
$route['users/getProfileID'] = 'Users_controller/getProfileID';
$route['users/UserByCompany'] = 'Users_controller/UserByCompany';
$route['users/updateStatus'] = 'Users_controller/updateStatus';
$route['users/editUser'] = 'Users_controller/editUser';
$route['users/profile_files'] = 'Users_controller/profile_files';
$route['users/DashboardCount'] = 'Users_controller/DashboardCount';
$route['users/resendConfirm'] = 'Users_controller/resendConfirm';

$route['applicant/apply_job'] = 'Applicant_controller/apply_job';
$route['applicant/favorite_job'] = 'Applicant_controller/favorite_job';
$route['applicant/getprofile'] = 'Applicant_controller/getProfile';
$route['applicant/edit_applicant'] = 'Applicant_controller/edit_applicant';
$route['applicant/getAnnounceMe'] = 'Applicant_controller/getAnnounceMe';
$route['applicant/chk_citizen'] = 'Applicant_controller/chk_citizen';
$route['applicant/update_applicant'] = 'Applicant_controller/update_applicant';

$route['irecruit/chk_iRecruit_get'] = 'Applicant_controller/chk_iRecruit';
$route['irecruit/register_iRecruit_post'] = 'Applicant_controller/register_iRecruit';
$route['irecruit/chk_position_iRecruit_post'] = 'Applicant_controller/chk_position_iRecruit';
$route['irecruit/chk_announce_iRecruit_post'] = 'Applicant_controller/chk_announce_iRecruit';



$route['master/ms_province_get'] = 'Master_controller/ms_province';
$route['master/ms_aumphur_get'] = 'Master_controller/ms_aumphur';
$route['master/ms_tumbon_get'] = 'Master_controller/ms_tumbon';
$route['master/ms_level_get'] = 'Master_controller/ms_level';
$route['master/ms_job_description_1_get'] = 'Master_controller/ms_job_description_1';
$route['master/ms_job_description_2_get'] = 'Master_controller/ms_job_description_2';
$route['master/ms_position_get'] = 'Master_controller/ms_position';
$route['master/ms_staff_get'] = 'Master_controller/ms_staff';
$route['master/ms_zone_get'] = 'Master_controller/ms_zone';
$route['master/ms_job_type_get'] = 'Master_controller/ms_job_type';
$route['master/ms_gender_get'] = 'Master_controller/ms_gender';
$route['master/ms_license_get'] = 'Master_controller/ms_license';
$route['master/ms_education_get'] = 'Master_controller/ms_education';
$route['master/ms_working_day_get'] = 'Master_controller/ms_working_day';
$route['master/ms_company_kind_get'] = 'Master_controller/ms_company_kind';
$route['master/ms_company_name_get'] = 'Master_controller/ms_company_name';
$route['master/ms_information_get'] = 'Master_controller/ms_information';
