<!DOCTYPE html>
  <html>
      <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      </head>
      <body style="margin:0px;padding:0px;">
        <div style="width:100%;height:100%;background:#eceff1;position:absolute;text-align:center;">
              <div style="box-shadow:0px 1px 1px 0px rgba(0,0,0,.1);position:absolute;width:90%;height:calc(100% - 100px);margin-top:50px;margin-left:5%;background:#fff;">
                   <div  style= "height:150px; width:100%; color:#fff;
                                font-family: 'Prompt', sans-serif; text-align:center;
                                font-weight:light; background-color:#fff;">
                                <!-- <img style="margin-top:10px;" height="120px"
                                src="http://203.151.43.169/digidoc/assets/images/logo/logo_wha.png"> -->
                   </div>
                   <div style="text-align:left;width:100%;color:#455a64;">
                        <h1 style="width:100%; display:block; text-align:center; color:#37474f;">ยืนยันอีเมลแอดเดรสของคุณ</h1>
                        <p style="margin:5px 30px;">
                          เรียน  <?php echo $name; ?>, <br />
                        </p>
                       <p style="margin:15px 30px;">คุณได้เข้าร่วมใช้งาน JOB ในขั้นตอนสุดท้ายของการสร้างบัญชีใช้งาน JOB ของคุณ
                       เราจำเป็นต้องตรวจสอบให้แน่ใจว่าอีเมลแอดเดรสนี้เป็นของคุณ
                        <br /> ในการยืนยันอีเมลแอดเดรสของคุณ คลิกที่ปุ่มยืนยันด้านล่าง โดยใช้ชื่อผู้ใช้นี้เผื่อเข้าสู่ระบบ</p>
                       <br />
                       <p style="text-align:center; width:100%">
                           Username : <?php echo $username; ?><br />
                       </p><br />
                       <p style="text-align:center; margin-top:20px; margin-bottom:30px;">
                          <a href="<?php echo $url; ?>" style="text-decoration:none; border-radius:5px; color:#fff;padding:20px 30px;background: #5babe8; ">ยืนยันและเปลี่ยนรหัสผ่าน</a>
                       </p><br />
                       <p style="margin:15px 30px;">
                        หากคุณไม่ได้ร้องขอ คุณสามารถเพิกเฉยต่ออีเมลนี้ได้อย่างปลอดภัย อาจมีผู้อื่นป้อนอีเมลแอดเดรสของคุณด้วยความผิดพลาด
                       </p>

                       <p style="margin:5px 30px;">
                        ขอแสดงความนับถือ <br />
                        JOB by Siamrajathanee
                      </p>

                   </div>
             <hr />
             <div style="text-align:left;width:100%;color:#455a64;">
               <p style="text-align:center; padding:20px;">
                  หากมีข้อสงสัยหรือสอบถามเพิ่มเติม ติดต่อ: <a href=<?php echo "http://".$_SERVER['SERVER_NAME']."/JOB_UI" ?> >Help Center</a>
               </p>
            </div>
        </div>
        <div style="position:absolute;bottom:0; width:100%; text-align:center; left:0; padding:10px 0px;">
              <p style="color:#546e7a;">
                Thailand | Siamrajathanee co., ltd Sumutpraparn 10130 | Copyright &copy; 2019
              </p>
        </div>
  	 </div>
		</body>
</html>
