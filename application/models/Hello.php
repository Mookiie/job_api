<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Hello extends CI_Model{

  function __construct()
  {
    parent::__construct();
  }

  public function hello()
  {
  	// config/autoload.php -> add file -> $autoload['model'] = array('hello');
  	$callback = array(
  						'data1' => "hello",
  						'data2'	=> "world"
  					  );
  	return $callback;
  }

}