<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Functions extends CI_Model{

  function __construct()
  {
    parent::__construct();
    $this->db_job = $this->load->database('Job',TRUE);
    $this->now = $this->date_time_get();
  }

  public function date_time_get()
  {
      date_default_timezone_set("Asia/Bangkok");
      $now = date("Y-m-d H:i:s");
      return $now;
  }

  public function GetIPGetAgent()
  {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    $agent = $_SERVER["HTTP_USER_AGENT"];
    return array('ip' => $ip,'agent' => $agent);
  }

  public function insertLog($company_id,$uid,$passive, $log_type, $log_msg, $log_from,$infor)
  {
        $log_id = time().rand(00001,99999);
        $log_ip = $this->GetIPGetAgent()['ip'];
        $log_agent = $this->GetIPGetAgent()['agent'];
        $INSERT_SQLLOG = "INSERT INTO log_system
                          VALUES ('$log_id', '$company_id', '$uid', '$passive', '$log_type', '$log_msg', '$log_ip', '$log_agent', '$this->now', '$log_from','$infor')";
        $INSERT_QRYLOG = $this->db_job->query($INSERT_SQLLOG);
        return $log_id;
  }

  public function get_path()
  {
    $path = "http://".$_SERVER['SERVER_NAME']."/JOB_API/";
    return $path;
  }
  public function get_pathUI()
  {
    $path = "http://".$_SERVER['SERVER_NAME']."/JOB_UI/";
    return $path;
  }

  public function randomString($length)
  {
      	$str = "";
      	$characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
      	$max = count($characters) - 1;
      	for ($i = 0; $i < $length; $i++) {
      		$rand = mt_rand(0, $max);
      		$str .= $characters[$rand];
      	}
      	return $str;
  }

  public function FnID($var)
  {
    	$srt[0] = substr($var, 0, 1);
    	$srt[1] = substr($var, 1, 4);
    	$srt[2] = substr($var, 5, 5);
    	$srt[3] = substr($var, 10, 2);
    	$srt[4] = substr($var, 12, 1);
    	return $srt[0]."-".$srt[1]."-".$srt[2]."-".$srt[3]."-".$srt[4];
  }

  public function GenerateGUID()
  {
    	$newGuid = sprintf('%02X%04X-%04X-%04X-%04X-%03X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));

        if (function_exists('uniqid') === true)
        {
            return $newGuid.uniqid('',true);
        }

        return $newGuid;
  }

  function GeneratePassword($len)
  {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@$%&*_.";
    $password = substr( str_shuffle( $chars ),0,$len );
    return $password;
  }

  public function insertLogUser($uid,$action_id,$passive,$page,$job_type,$job_description_1,$job_description_2,$zone,$education,$company,$infor)
  {
        $log_id = time().rand(00001,99999);
        $log_ip = $this->GetIPGetAgent()['ip'];
        $log_agent = $this->GetIPGetAgent()['agent'];
        $INSERT_SQLLOG = "INSERT INTO log_user
                          VALUES ('$log_id', '$this->now', '$log_ip', '$log_agent', '$uid', '$action_id', '$passive', '$page', '$job_type', '$job_description_1', '$job_description_2', '$zone', '$education','$company','$infor')";
        $INSERT_QRYLOG = $this->db_job->query($INSERT_SQLLOG);
        if($INSERT_QRYLOG){
          $callback = array(
           "status" => 200,
           "type" => TRUE,
           "msg" => "OK",
           "id" => $log_id
          );
        }
        else{
         $callback = array(
           "status" => 201,
           "type" => FALSE,
           "msg" => "Query Error",
           "data" => $INSERT_SQLLOG
          );
        }
        return $callback;
  }


}
