<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Users extends CI_Model{

  function __construct()
  {
    parent::__construct();
    $this->db_job = $this->load->database('Job',TRUE);
    $this->now = $this->Functions->date_time_get();
    $this->path = $this->Functions->get_path();
  }

  private function GenerateUID()
  {
    $query = "SELECT uid FROM users ORDER BY uid DESC LIMIT 1";
    $qryid = $this->db_job->query($query);
    if($qryid->num_rows()>0){
      $userId = $qryid->result_array();
      $oldId = $userId[0]["uid"];
      $oldYMD =  substr($oldId,1,6);
      $y = sprintf("%02d",date('y'));
      $m = sprintf("%02d",date('m'));
      $d = sprintf("%02d",date('d'));
      $newYMD =  $y.$m.$d;
      $oldNUM =  sprintf("%06d",substr($oldId,7,6));
      if ($oldYMD == $newYMD) {
          $newNUM = $oldNUM+1;
          $newNUM = sprintf("%06d",$newNUM);
      }else{
          $newNUM = "000001";
      }
      return "U".$newYMD.$newNUM;
    }else{
      $y = sprintf("%02d",date('y'));
      $m = sprintf("%02d",date('m'));
      $d = sprintf("%02d",date('d'));
      $newYMD =  $y.$m.$d;
      $newNUM = "000001";
      return "U".$newYMD.$newNUM;
    }
  }

  public function create($company_id, $fname, $lname, $username, $tel, $level, $invite_by)
  {
    $chk_duplicate = $this->chkEmailDuplicate($username);
    if ($chk_duplicate['status'] == '202') {
      return $chk_duplicate;
    }
    else {
      $uid = $this->GenerateUID();
      $status = "9";
      $invite_at = $this->now;
      $password = md5($this->Functions->GeneratePassword(8));
      $sql = "INSERT INTO users
              VALUES('$uid', '$fname', '$lname', '$username', '$password', '$tel', '$level','user.png', '$status', '$company_id', '$invite_at', '$invite_by','', '')";
      $qry  = $this->db_job->query($sql);
      if($qry){
          $this->Email->SendMailConfirm($uid);
          $this->Functions->insertLog($company_id,$uid,$invite_by,"User Register", "ลงทะเบียนผู้ใช้", "users","");
        $callback = array(
         "status" => 200,
         "type" => TRUE,
         "msg" => "OK",
         "id" => $uid
        );
      }else{
       $callback = array(
         "status" => 201,
         "type" => FALSE,
         "msg" => "Query Error",
         "data" => $sql
        );
      }
      return $callback;
    }
  }

  public function edit($fname, $lname, $tel, $uid, $update_by,$company_id)
  {
      $sql="UPDATE users
            SET fname = '$fname',lname = '$lname', tel = '$tel', update_by = '$update_by', update_at = '$this->now'
            WHERE uid = '$uid'";
      $qry  = $this->db_job->query($sql);
      if($qry){
        $this->Functions->insertLog($company_id,$uid,$update_by,"User Edit", "แก้ไขข้อมูลา่วนตัว", "users","");
        $callback = array(
         "status" => 200,
         "type" => TRUE,
         "msg" => "OK",
         "id" => $uid
        );
      }else{
       $callback = array(
         "status" => 201,
         "type" => FALSE,
         "msg" => "Query Error",
         "data" => $sql
        );
      }
      return $callback;
  }

  public function file_profile($id,$file)
  {
    $imgname = $file["name"][0];
    $tmp = $file["tmp_name"][0];
    $image_array = explode(".",$imgname);
    $type = $image_array[count($image_array)-1];
    if ($type!="png" || $type!="jpg" || $type!="jpeg" || $type!="gif" ) {
      $newname = $id."_".time().".".$type;
      if (move_uploaded_file($tmp,"assets/images/profiles/".$newname)){
        $sql = "UPDATE users SET image = '$newname' WHERE uid ='$id' ";
        $qry = $this->db_job->query($sql);
        if ($qry) {
          $callback = array(
            "status" => 200,
            "type" => TRUE,
            "msg" => "OK",
          );
        }else {
          $callback = array(
            "status" => 400,
            "type" => FALSE,
            "msg" => "Insert Failed",
          );
        }
      }else {
        $callback = array(
          "status" => 400,
          "type" => FALSE,
          "msg" => "Upload Failed",
        );
      }
    }else {
      $callback = array(
        "status" => 406,
        "type" => FALSE,
        "msg" => "Type Not Support",
      );
    }
    return $callback;
  }

  public function updateCreateCompany($company_id,$uid)
  {
    $sql="UPDATE users
          SET company_id = '$company_id'
          WHERE uid = '$uid'";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $this->Functions->insertLog($company_id,$uid,$company_id,"Conmpany Register", "สร้างบริษัท", "company","");
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }

  public function updatePassword($uid,$password,$update_by)
  {
    $sql="UPDATE users
          SET password = '$password',
          update_at = '$this->now',
          update_by = '$update_by'
          WHERE uid = '$uid'";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }

  public function updateLevel($uid,$level)
  {
    $sql="UPDATE users
          SET level = '$level'
          WHERE uid = '$uid'";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }

  public function updateStatus($uid,$status,$update_by)
  {
    $sql="UPDATE users
          SET status = '$status',
              update_at = '$this->now',
              update_by = '$update_by'
          WHERE uid = '$uid'";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }

  public function getUserById($uid)
  {
    $sql = "SELECT * FROM users WHERE uid = '$uid'";
    $qry  = $this->db_job->query($sql);
    if ($qry->num_rows() > 0) {
      		$callback = array(
                            "status" => 200,
                            "type" => TRUE,
              						  "msg" => "OK",
              						  "data" => $qry->result_array()[0]
              						 );
    }else{
      		$callback = array(
                            "status" => 404,
                            "type" => FALSE,
                            "msg" => "Not Found",
                            "data" => "$sql"
                           );
    }
    return $callback;
  }

  public function getUserByEmail($email)
  {
    $sql = "SELECT * FROM users WHERE email = '$email'";
    $qry  = $this->db_job->query($sql);
    if ($qry->num_rows() > 0) {
      		$callback = array(
                            "status" => 200,
                            "type" => TRUE,
              						  "msg" => "OK",
              						  "data" => $qry->result_array()[0]
              						 );
    }else{
      		$callback = array(
                            "status" => 404,
                            "type" => FALSE,
                            "msg" => "Not Found",
                            "data" => "$sql"
                           );
    }
    return $callback;
  }

  public function chkEmailDuplicate($email)
  {
    $sql = "SELECT * FROM users WHERE email = '$email' AND status = '1'";
    $qry  = $this->db_job->query($sql);
    if ($qry->num_rows() > 0) {
      $callback = array(
                        "status" => 202,
                        "type" => FALSE,
                        "msg" => "Data Duplicate",
                        "data" => ""
                       );

    }else{
      $callback = array(
                        "status" => 200,
                        "type" => TRUE,
                        "msg" => "OK",
                        "data" => ""
                       );
    }
    return $callback;

  }

  public function chkPassCode($uid,$code)
  {
    $sql = "SELECT * FROM users WHERE uid = '$uid' AND password = '$code'";
    $qry  = $this->db_job->query($sql);
    $rowuser = $qry->result_array()[0];
    if ($rowuser['status'] != 9) {
      $callback = array(
                        "status" => 304,
                        "type" => FALSE,
                        "msg" => "Comfirm",
                        "data" => ""
                       );
      return $callback;
    }
    if ($qry->num_rows() > 0) {
      $callback = array(
                        "status" => 200,
                        "type" => TRUE,
                        "msg" => "OK",
                        "data" => ""
                       );

    }else{
      $callback = array(
                        "status" => 404,
                        "type" => FALSE,
                        "msg" => "Data NotFound",
                        "data" => ""
                       );
    }
    return $callback;
  }

  public function chkStatus($uid)
  {
    $sql = "SELECT * FROM users WHERE uid = '$uid'";
    $qry  = $this->db_job->query($sql);
    $rowuser = $qry->result_array()[0];
    if ($rowuser['status'] != 9) {
      $callback = array(
                        "status" => 304,
                        "type" => FALSE,
                        "msg" => "Comfirm",
                        "data" => ""
                       );
    }else{
      $callback = array(
                        "status" => 404,
                        "type" => FALSE,
                        "msg" => "Data NotFound",
                        "data" => ""
                       );
    }
    return $callback;
  }

  public function getProfile($uid)
  {
    $sql = "SELECT uid, fname, lname, email, tel, level, image, status, company_id, invite_at, invite_by, update_at, update_by FROM users WHERE uid = '$uid'";
    $qry = $this->db_job->query($sql);
     if ($qry->num_rows() > 0) {
      $rowuser = $qry->result_array()[0];
      $company = ($rowuser['company_id'] == null ? '' : $this->Company->getcompnyById($rowuser["company_id"])[0]);
      $level = $this->Master->getLvById($rowuser["level"])[0];
      $rowuser['company'] = $company;
      $rowuser['level'] = $level;
      $rowuser['image'] = $this->path."assets/images/profiles/".$rowuser['image'];
      $rowuser['invite_by'] = ($rowuser['invite_by'] == 'U000000000000' ? '' : $this->getUserById($rowuser['invite_by']));
      $rowuser['update_by'] = ($rowuser['update_by'] == 'U000000000000' ? '' : $this->getUserById($rowuser['update_by']));
      $callback = array(
                          "status" => 200,
                          "type" => TRUE,
                          "msg" => "OK",
                          "data" => $rowuser
                         );
    }else{
      $callback = array(
                          "status" => 404,
                          "type" => FALSE,
                          "msg" => "Not Found",
                          "data" => ""
                        );
    }
    return $callback;
  }

  public function getUserByCompany($company_id)
  {
    $sql = "SELECT uid, fname, lname, email, tel, level, image, status, company_id, invite_at, invite_by, update_at, update_by FROM users WHERE uid <> 'U000000000000'";
    if ($company_id != '') {
      $sql .= "AND company_id = '$company_id'";
    }
    $qry  = $this->db_job->query($sql);
    $rowuser = $qry->result_array();
    if ($qry->num_rows() > 0) {
      for ($i=0; $i < $qry->num_rows(); $i++) {
        $rowuser[$i]['image'] = $this->path."assets/images/profiles/".$rowuser[$i]['image'];
      }
      $callback = array(
                        "status" => 200,
                        "type" => TRUE,
                        "msg" => "OK",
                        "data" => $rowuser
                       );
    }else{
      $callback = array(
                        "status" => 404,
                        "type" => FALSE,
                        "msg" => "Data NotFound",
                        "data" => ""
                       );
    }
    return $callback;
  }

  public function DashboardCount($company_id)
  {
    $sql_all = "SELECT * FROM users ";
    if ($company_id != '') {
      $sql_all .= "WHERE company_id = '$company_id'";
    }
    $qry_all  = $this->db_job->query($sql_all);

    $sql_online = "SELECT * FROM users WHERE status in ('1','9') ";
    if ($company_id != '') {
      $sql_online .= "AND company_id = '$company_id'";
    }
    $qry_online  = $this->db_job->query($sql_online);

    $sql_offline = "SELECT * FROM users WHERE status = '0'";
    if ($company_id != '') {
      $sql_offline .= "AND company_id = '$company_id'";
    }
    $qry_offline  = $this->db_job->query($sql_offline);
    $dataset = array(
                      "all"=>$qry_all->num_rows(),
                      "online"=>$qry_online->num_rows(),
                      "offline"=>$qry_offline->num_rows()
                    );
    if ($qry_all) {
      $callback = array(
                        "status" => 200,
                        "type" => TRUE,
                        "msg" => "OK",
                        "data" => $dataset
                       );
    }else{
      $callback = array(
                        "status" => 404,
                        "type" => FALSE,
                        "msg" => "Data NotFound",
                        "data" => ""
                       );
    }
    return $callback;


  }


}
