<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//Load Composer's autoloader
require 'vendor/autoload.php';

// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Email extends CI_Model{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Functions');
    $this->now = $this->Functions->date_time_get();
    $this->pathUI = $this->Functions->get_pathUI();
    $this->path = $this->Functions->get_path();

  }

  public function send_mail($listTo,$subject,$listMsg)
  {
    $mail = new PHPMailer(true);                                // Passing `true` enables exceptions
      try {
          //Server settings
          $mail->SMTPDebug = 0;                                 // Enable verbose debug output
          $mail->isSMTP();                                      // Set mailer to use SMTP
          $mail->Host = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
          $mail->SMTPAuth = true;                               // Enable SMTP authentication
          $mail->CharSet = "utf-8";
          $mail->Username = 'info.carpool.2017@gmail.com';       // SMTP username
          $mail->Password = 'mook0601;';                        // SMTP password
          $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
          $mail->Port = 465;                                    // TCP port to connect to
          $mail->setFrom('info.carpool.2017@gmail.com', 'SERVICE');
          //Recipients
          for ($i=0; $i <count($listTo) ; $i++) {
            $mail->addAddress($listTo[$i]);     // Add a recipient
          }
          //Attachments
          //Content
          $mail->isHTML(true);                                  // Set email format to HTML
          $mail->Subject = $subject;
          for ($m=0; $m <count($listMsg) ; $m++) {
            $mail->Body = $listMsg[$m];
          }
          // $mail->Body    =  $message;
          $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            if($mail->send()){
                $callback = array(
                  "status" => 200,
                  "msg" => "OK",
                  "type" => TRUE
              );
              return $callback;
            }else{
              $callback = array(
                "status" => 300,
                "msg" =>  show_error($this->email->print_debugger()),
                "type" => FALSE
              );
              return $callback;
            }
      } catch (Exception $e) {
          // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
      }
  }

  public function SendMailConfirm($Id)
  {
  	$listEmail = array();
  	$listHtml = array();
    $rowuser = $this->Users->getUserById($Id)["data"];
      $data["name"] = $rowuser['fname']." ".$rowuser['lname'];
    	$data["username"] = $rowuser['email'];
      $uid = base64_encode(base64_encode(base64_encode($rowuser["uid"])));
      $data["url"]= $this->pathUI.'confirm?id='.$uid."&code=".$rowuser["password"];
    	$html = $this->load->view('formmail/user_create.php', $data,true);
    	array_push($listEmail,$rowuser["email"]);
    	array_push($listHtml,$html);
  	$callback = $this->Email->send_mail($listEmail,'Please verify your email',$listHtml);
  	return $callback;
  }


}
