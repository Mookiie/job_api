<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Applicant extends CI_Model{

  function __construct()
  {
    parent::__construct();
    $this->db_job = $this->load->database('Job',TRUE);
    $this->db_recruitment= $this->load->database('Recruitment',TRUE);
    $this->now = $this->Functions->date_time_get();
    $this->path = $this->Functions->get_path();

  }

  public function add($uid, $citizen_id, $pfix_code, $fname, $lname, $fname_eng, $lname_eng, $nationality,
    $ethnicity, $religion, $position, $job_description_code_1, $job_description_code_2, $required_salary,
    $mobile,$birth_date, $weight, $height, $married_status, $no_of_children, $current_address,
    $current_number, $current_moo, $current_alley, $current_road, $current_tumbon_code,
    $current_amphur_code, $current_province_code, $current_zipcode, $census_address,
    $census_number, $census_moo, $census_alley, $census_road, $census_tumbon_code,
    $census_amphur_code, $census_province_code, $census_zipcode, $sex, $degree,
    $institute, $finsh_year, $grade, $status, $st_interview, $st_crime,
    $st_ability, $st_disc, $st_test, $st_drive, $st_training, $st_interviewCI,$create_by )
  {
    $sql = "INSERT INTO resume
            VALUES(
                $uid, $citizen_id, $pfix_code, $fname, $lname, $fname_eng, $lname_eng, $nationality,
                $ethnicity, $religion, $position, $job_description_code_1, $job_description_code_2, $required_salary,
                $mobile,$birth_date, $weight, $height, $married_status, $no_of_children, $current_address,
                $current_number, $current_moo, $current_alley, $current_road, $current_tumbon_code,
                $current_amphur_code, $current_province_code, $current_zipcode, $census_address,
                $census_number, $census_moo, $census_alley, $census_road, $census_tumbon_code,
                $census_amphur_code, $census_province_code, $census_zipcode, $sex, $degree,
                $institute, $finsh_year, $grade, $status, $st_interview, $st_crime,
                $st_ability, $st_disc, $st_test, $st_drive, $st_training, $st_interviewCI,
                '$this->now', $create_by, '', '',''
            )";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $this->Functions->insertLog($company_id,$create_by,$announce_id,"Add User", "เพิ่มประกาศ", "announce","");
      $callback = array(
       "status" => 200,
       "type" => TRUE,
       "msg" => "OK",
       "id" => $announce_id
      );
    }else{
     $callback = array(
       "status" => 201,
       "type" => FALSE,
       "msg" => "Query Error",
       "data" => $sql
      );
    }
    return $callback;

  }

  public function updateImg($uid,$image)
  {
    $sql="UPDATE applicant
          SET image = '$image'
          WHERE uid = '$uid'";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }

  public function updateStatus($status,$uid)
  {
    $sql="UPDATE applicant
          SET status = '$status'
          WHERE uid = '$uid'";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $this->Functions->insertLog("",$uid,"","Update Status Applicant", "แก้ไขสถานะ", "Applicant","");
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }
  public function update_applicant($uid,$citizen_id,$province,$aumphur,$birth_date,$tel)
  {
    $citizen_id = str_replace("-","",$citizen_id);
    $sql="UPDATE applicant
          SET citizen_id = '$citizen_id',
              province = '$province',
              aumphur = '$aumphur',
              birth_date = '$birth_date',
              tel = '$tel'
          WHERE uid = '$uid'";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $this->Functions->insertLog("",$uid,"","Update citizen_id Applicant", "แก้ไขเลขบัตร", "Applicant","");
      $this->chk_citizen($uid,$citizen_id);
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }

  public function edit_applicant($uid,$citizen_id,$province,$aumphur,$birth_date,$tel,$email)
  {
    $citizen_id = str_replace("-","",$citizen_id);
    $sql="UPDATE applicant
          SET citizen_id = '$citizen_id',
          province = '$province',
          aumphur = '$aumphur',
          birth_date = '$birth_date',
          tel = '$tel',
          email = '$email'
          WHERE uid = '$uid'";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $this->Functions->insertLog("",$uid,"","Update citizen_id Applicant", "แก้ไขเลขบัตร", "Applicant","");
      $this->chk_citizen($uid,$citizen_id);
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }

  public function Chk_applicant($uid)
  {
    $sql = "SELECT * FROM applicant WHERE uid = '$uid'";
    $qry  = $this->db_job->query($sql);
    if($qry->num_rows() == 0){
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
      );
    }else{
      $callback = array(
        "status" => 202,
        "type" => FALSE,
        "msg" => "Data Duplicate",
        "data" => $qry->result_array()
      );
    }
    return $callback;
  }

  public function Chk_resume($citizen_id)
  {
    $sql = "SELECT * FROM z_hr_recruitment_header WHERE id_card = '$citizen_id'";
    $qry  = $this->db_recruitment->query($sql);
    if($qry->num_rows() > 0){
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "data" => $qry->result_array()
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }

  public function applicant_register($uid,$fname,$lname,$tel,$email,$citizen_id,$status,$img)
  {
    $citizen_id = str_replace("-","",$citizen_id);
    $sql = "INSERT INTO applicant
            VALUES(
              '$uid','$citizen_id','$fname','$lname','$tel','$email','$this->now','$status','$img','','',''
            )";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $this->Functions->insertLog("",$uid,"","Register Applicant", "ลงทะเบียนใช้งาน", "Applicant","");
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error"
      );
    }
    return $callback;
  }

  public function getApplicantByUid($uid)
  {
    $sql_uid= "SELECT * FROM applicant WHERE uid = '$uid'";
    $qry_uid  = $this->db_job->query($sql_uid);
    $citizen_id = $qry_uid->result_array()[0]['citizen_id'];
    $sql = "SELECT a.*,b.*,c.zone_id ,(DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(a.birth_date, '%Y')) - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(a.birth_date, '00-%m-%d')) AS ages
     FROM z_hr_recruitment_header a
            LEFT JOIN (
              SELECT
                z_hr_recruitment_education.run_no AS run_no,
                z_hr_recruitment_education.id_card AS id_card,
                z_hr_recruitment_education.institute AS institute,
                z_hr_recruitment_education.major AS major,
                z_hr_recruitment_education.finish_year AS finish_year,
                z_hr_recruitment_education.grade AS grade,
                MAX(z_hr_recruitment_education.degree_code) AS degree_code
              FROM
                 z_hr_recruitment_education
              GROUP BY z_hr_recruitment_education.id_card) b ON a.run_no = b.run_no
              LEFT JOIN z_hr_recruitment_interview c ON a.run_no = c.run_no
              LEFT JOIN ms_zone cc ON c.zone_id = cc.zone_id
            WHERE a.id_card = '$citizen_id' AND a.status = 'A'";
    $qry  = $this->db_recruitment->query($sql);

    if($qry->num_rows()>0){
      $dataset = $qry->result_array()[0];
      $dataset['uid'] = $uid;
      $dataset['fname'] = $qry_uid->result_array()[0]['fname'];
      $dataset['lname'] = $qry_uid->result_array()[0]['lname'];
      $dataset['image'] = $qry_uid->result_array()[0]['image'];
      $dataset['tel'] = $qry_uid->result_array()[0]['tel'];
      $dataset['citizen'] = $qry_uid->result_array()[0]['citizen_id'];
      $dataset['email'] = $qry_uid->result_array()[0]['email'];
      $dataset['AP_status'] = $qry_uid->result_array()[0]['status'];
      $dataset["job_description_1"] = $this->Master->getjob_description_1ById($dataset['job_description_code_1']);
      $dataset["job_description_2"] = $this->Master->getjob_description_2ById($dataset['job_description_code_1'],$dataset['job_description_code_2']);
      if ($dataset['degree_code']!='') {
        $dataset["degree"] = $this->Master->geteducationById($dataset['degree_code'])[0];
        $dataset["degree"]['institute'] = $qry->result_array()[0]['institute'];
        $dataset["degree"]['major'] = $qry->result_array()[0]['major'];
        $dataset["degree"]['finish_year'] = $qry->result_array()[0]['finish_year'];
        $dataset["degree"]['grade'] = $qry->result_array()[0]['grade'];
      }
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "data" => $dataset,
        "count" =>$qry->num_rows()
      );
    }else{
      $dataset = array();
      $dataset['uid'] = $uid;
      $dataset['id_card'] = $qry_uid->result_array()[0]['citizen_id'];
      $dataset['fname'] = $qry_uid->result_array()[0]['fname'];
      $dataset['lname'] = $qry_uid->result_array()[0]['lname'];
      $dataset['tel'] = $qry_uid->result_array()[0]['tel'];
      $dataset['email'] = $qry_uid->result_array()[0]['email'];
      $dataset['AP_status'] = $qry_uid->result_array()[0]['status'];
      $dataset['image'] = $qry_uid->result_array()[0]['image'];
      $dataset['tel'] = $qry_uid->result_array()[0]['tel'];
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $dataset,
        "citizen" =>$citizen_id,
        "count" =>$qry->num_rows()
      );
    }
    return $callback;
  }

  // public function getApplicantByCitizen_id($citizen_id)
  // {
  //   $sql = "SELECT a.*,b.degree_code,c.zone_id  FROM z_hr_recruitment_header a
  //           LEFT JOIN (
  //             SELECT
  //               z_hr_recruitment_education.run_no AS run_no,
  //               z_hr_recruitment_education.id_card AS id_card,
  //               MAX(z_hr_recruitment_education.degree_code) AS degree_code
  //             FROM
  //                z_hr_recruitment_education
  //             GROUP BY z_hr_recruitment_education.id_card) b ON a.run_no = b.run_no
  //             LEFT JOIN z_hr_recruitment_interview c ON a.run_no = c.run_no
  //             LEFT JOIN ms_zone cc ON c.zone_id = cc.zone_id
  //           WHERE a.id_card = '$citizen_id' AND a.status = 'A'";
  //   $qry  = $this->db_recruitment->query($sql);
  //
  //   if($qry->num_rows()>0){
  //     $dataset = $qry->result_array()[0];
  //     $callback = array(
  //       "status" => 200,
  //       "type" => TRUE,
  //       "msg" => "OK",
  //       "data" => $dataset,
  //       "count" =>$qry->num_rows()
  //     );
  //   }else{
  //     $sql_uid= "SELECT * FROM applicant WHERE citizen_id = '$citizen_id'";
  //     $qry_uid  = $this->db_job->query($sql_uid);
  //     $dataset = $qry_uid->result_array()[0];
  //     $callback = array(
  //       "status" => 200,
  //       "type" => TRUE,
  //       "msg" => "OK",
  //       "data" => $dataset,
  //       "count" =>$qry->num_rows()
  //     );
  //   }
  //   return $callback;
  // }

  public function getProfile($uid)
  {
    $sql_uid= "SELECT * FROM applicant WHERE uid = '$uid'";
    $qry_uid  = $this->db_job->query($sql_uid);
    $citizen_id = $qry_uid->result_array()[0]['citizen_id'];
    $sql = "SELECT a.*,b.*,h.*,c.zone_id ,(DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(a.birth_date, '%Y')) - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(a.birth_date, '00-%m-%d')) AS ages
     FROM z_hr_recruitment_header a
            LEFT JOIN (
              SELECT
                z_hr_recruitment_education.run_no AS run_no,
                z_hr_recruitment_education.id_card AS id_card,
                z_hr_recruitment_education.institute AS institute,
                z_hr_recruitment_education.major AS major,
                z_hr_recruitment_education.finish_year AS finish_year,
                z_hr_recruitment_education.grade AS grade,
                MAX(z_hr_recruitment_education.degree_code) AS degree_code
              FROM
                 z_hr_recruitment_education
              GROUP BY z_hr_recruitment_education.id_card) b ON a.run_no = b.run_no
              LEFT JOIN z_hr_recruitment_interview c ON a.run_no = c.run_no
              LEFT JOIN ms_zone cc ON c.zone_id = cc.zone_id
              LEFT JOIN hr_staff h ON a.id_card = h.staff_id
            WHERE a.id_card = '$citizen_id' AND a.status = 'A'";
    $qry  = $this->db_recruitment->query($sql);
    if ($qry->num_rows() > 0 ) {
      $dataset = $qry->result_array()[0];
      $dataset['uid'] = $uid;
      $dataset['id_card'] = $qry_uid->result_array()[0]['citizen_id'];
      $dataset['image'] = $qry_uid->result_array()[0]['image'];
      $dataset['tel'] = $qry_uid->result_array()[0]['tel'];
      $dataset['email'] = $qry_uid->result_array()[0]['email'];
      $dataset['birth_date'] = $qry_uid->result_array()[0]['birth_date'];
      $dataset['AP_status'] = $qry_uid->result_array()[0]['status'];
      $dataset['province'] = $qry_uid->result_array()[0]['province'];
      $dataset['aumphur'] = $qry_uid->result_array()[0]['aumphur'];
      $dataset["nationality_code_1"] = $this->Master->getms_nationalityById($dataset['nationality_code_1']);
      $dataset["pfix_code"] = $this->Master->getms_pfixById($dataset['pfix_code']);
      $dataset["married_status"] = $this->Master->getms_marital_statusById($dataset['married_status']);
      $dataset["sex"] = $this->Master->getms_genderById($dataset['sex']);
      $dataset["job_description_1"] = $this->Master->getjob_description_1ById($dataset['job_description_code_1']);
      $dataset["job_description_2"] = $this->Master->getjob_description_2ById($dataset['job_description_code_1'],$dataset['job_description_code_2']);
      if ($dataset['degree_code']!='') {
        $dataset["degree"] = $this->Master->geteducationById($dataset['degree_code'])[0];
        $dataset["degree"]['institute'] = $qry->result_array()[0]['institute'];
        $dataset["degree"]['major'] = $qry->result_array()[0]['major'];
        $dataset["degree"]['finish_year'] = $qry->result_array()[0]['finish_year'];
        $dataset["degree"]['grade'] = $qry->result_array()[0]['grade'];
      }
    }else {
      $dataset = array();
      $dataset['uid'] = $uid;
      $dataset['id_card'] = $qry_uid->result_array()[0]['citizen_id'];
      $dataset['fname'] = $qry_uid->result_array()[0]['fname'];
      $dataset['lname'] = $qry_uid->result_array()[0]['lname'];
      $dataset['tel'] = $qry_uid->result_array()[0]['tel'];
      $dataset['email'] = $qry_uid->result_array()[0]['email'];
      $dataset['birth_date'] = $qry_uid->result_array()[0]['birth_date'];
      $dataset['AP_status'] = $qry_uid->result_array()[0]['status'];
      $dataset['image'] = $qry_uid->result_array()[0]['image'];
      $dataset['province'] = $qry_uid->result_array()[0]['province'];
      $dataset['aumphur'] = $qry_uid->result_array()[0]['aumphur'];
    }

    if ($qry->num_rows()>0) {
      $callback = array(
                            "status" => 200,
                            "type" => TRUE,
                            "msg" => "OK",
                            "data" => $dataset
                           );
    }else {
      $callback = array(
                          "status" => 202,
                          "type" => FALSE,
                          "msg" => "Not Profile",
                          "data" => $dataset
                        );
    }
    return $callback;
  }

  public function updateimgLine($uid,$img)
  {
    $sql="UPDATE applicant
          SET image = '$img'
          WHERE uid = '$uid'";
    $qry  = $this->db_job->query($sql);
  }

  public function chk_citizen($uid,$citizen_id)
  {
    $sql= "SELECT * FROM applicant WHERE uid = '$uid'";
    $qry = $this->db_job->query($sql);
    $user = $qry->result_array()[0];

    if ($user['citizen_id'] == "" || $user['province'] == "" || $user['aumphur'] == ""
     || $user['birth_date'] == "" || $user['birth_date'] == "0000-00-00" || $user['tel'] == "") {
      $callback = array(
        "status" => 404,
        "type" => FALSE,
        "msg" => "Not Found",
        "citizen" => $user['citizen_id'],
        "province" => $user['province'],
        "aumphur" => $user['aumphur'],
        "birth_date" => $user['birth_date'],
        "tel" => $user['tel']
      );
    }else {
      $Chk_resume = $this->Chk_resume($citizen_id);
      if ($Chk_resume['status'] == '200') {
        $this->updateStatus('1',$uid);
      }
      $callback = array("status" => 200,
                        "type" => TRUE,
                        "msg" => "OK",
                        "citizen" => $user['citizen_id'],
                        "province"=> $user['province'],
                        "aumphur"=> $user['aumphur'],
                        "birth_date" => $user['birth_date']
                       );
    }
    return $callback;
  }

  // public function getAnnounceWithPercentMatch($citizen,$job_description_1,$job_description_2,$birth_date,$zone,$degree)
  // {
  //   $date = new DateTime($birth_date);
  //   $now = new DateTime();
  //   $interval = $now->diff($date);
  //   $age = $interval->y;
  //   $sql_announce = "SELECT * ,
  //                   IF((job_description_1 = '$job_description_1' AND '$job_description_1' != '' ), 1,  0)
  //           				+IF((job_description_2 = '$job_description_2' AND '$job_description_2' != '' ), 1, 0)
  //           				+IF((education = '$degree' AND '$degree' != '' ), 1,  0)
  //           				+IF((zone = '$zone' AND '$zone' != '' ), 1,  0)
  //           				+IF((('$age' >= age_min AND '$age' <= age_max)AND('$age' != '' AND '$age' != '') ), 1, 0) AS countmatch
  //                   FROM announce
  //                   JOIN company ON announce.company_id = company.company_id";
  //   $qry_announce  = $this->db_job->query($sql_announce);
  //   $dataset = $qry_announce->result_array();
  //   for ($i=0; $i < $qry_announce->num_rows(); $i++) {
  //     $dataset[$i]["percent"] = ($dataset[$i]['countmatch']*100)/5;
  //   }
  //   return $dataset;
  //
  // }

  public function getAnnounceWithPercentMatch(
    $citizen,$job_description_1,$job_description_2,$birth_date,$zone,$degree,
    $search_job_description_1,$search_job_description_2,$search_type_job,
    $search_zone,$search_education,$search_companyname,$offset)
  {

    $date = new DateTime($birth_date);
    $now = new DateTime();
    $interval = $now->diff($date);
    $age = $interval->y;
    $sql_announce = "SELECT announce.*,
                    IF((job_description_1 = '$job_description_1' AND '$job_description_1' != '' ), 1,  0)
            				+IF((job_description_2 = '$job_description_2' AND '$job_description_2' != '' ), 1, 0)
            				+IF((education = '$degree' AND '$degree' != '' ), 1,  0)
            				+IF((zone = '$zone' AND '$zone' != '' ), 1,  0)
            				+IF((('$age' >= age_min AND '$age' <= age_max)AND('$age' != '' AND '$age' != '') ), 1, 0) AS countmatch
                    FROM announce
                    LEFT JOIN company ON announce.company_id = company.company_id
                    LEFT JOIN company_detail  ON announce.company_id = company_detail.company_id
                    WHERE announce.status = '1'  AND company.company_status = '1'";
    if ($search_job_description_1 != '') {
      $sql_announce .= "AND job_description_1 = '$search_job_description_1'";
    }
    if ($search_job_description_2 != '') {
      $sql_announce .= "AND job_description_2 = '$search_job_description_2'";
    }
    if ($search_type_job != '') {
      $sql_announce .= "AND type_job = '$search_type_job'";
    }
    if ($search_zone != '') {
      $sql_announce .= "AND zone = '$search_zone'";
    }
    if ($search_education != '') {
      $sql_announce .= "AND education = '$search_education'";
    }
    if ($search_companyname != '') {
      $sql_announce .= "AND company_name LIKE '%$search_companyname%'";
    }
    $sql_announce .= "ORDER BY countmatch DESC, create_at DESC LIMIT 9999 OFFSET $offset";
    $qry_announce  = $this->db_job->query($sql_announce);
    $dataset = $qry_announce->result_array();
    for ($i=0; $i < $qry_announce->num_rows(); $i++) {
      $dataset[$i]["company"] = $this->Company->getDeail($dataset[$i]['company_id'])['data'];
      $dataset[$i]["percent"] = ($dataset[$i]['countmatch']*100)/5;
      $dataset[$i]["job_description_2"] = $this->Master->getjob_description_2ById($dataset[$i]['job_description_1'],$dataset[$i]['job_description_2']);
      $dataset[$i]["job_description_1"] = $this->Master->getjob_description_1ById($dataset[$i]['job_description_1']);
      $dataset[$i]["education"] = $this->Master->geteducationById($dataset[$i]['education']);
      $dataset[$i]["gender"] = $this->Master->getGenderById($dataset[$i]['gender']);
    }
    if ($qry_announce) {
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "count" =>$qry_announce->num_rows(),
        "data" => $dataset
      );
    }else {
      $callback = array(
        "status" => 400,
        "type" => FALSE,
        "msg" => "Insert Failed",
      );
    }
    return $callback;
  }

  public function GetActivityByUid($uid)
  {
    $sql = "SELECT *
            FROM db_job.log_activity_view
            WHERE uid = '$uid'
            ORDER BY log_date DESC";
    $qry = $this->db_job->query($sql);
    if ($qry) {
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "count" =>$qry->num_rows(),
        "data" => $qry->result_array()
      );
    }else {
      $callback = array(
        "status" => 400,
        "type" => FALSE,
        "msg" => "Insert Failed",
      );
    }
    return $callback;
  }

  public function CountActivityByUid($uid)
  {
    $sql = "SELECT *
            FROM db_job.log_activity_view
            WHERE uid = '$uid'
            ORDER BY log_date DESC";

    $qry = $this->db_job->query($sql);
    $data = $qry->result_array();
    $login = 0;
    $regis = 0;
    for ($i=0; $i < $qry->num_rows(); $i++) {
      // $data[$i];
      // print_r($data[$i]);exit;
      if ($data[$i]['action_id'] == "Login") {
        $login++;
      }elseif ($data[$i]['action_id'] == "2") {
        $regis++;
      }
      // elseif (condition) {
      //   // code...
      // }
    }
    $sql_login = "SELECT *
            FROM db_job.log_activity_view
            WHERE uid = '$uid' AND action_id = 'Login'
            ORDER BY log_date DESC limit 1";

    $qry_login = $this->db_job->query($sql_login);
    $sql_regis = "SELECT *
            FROM db_job.log_activity_view
            WHERE uid = '$uid' AND action_id = '2'
            ORDER BY log_date DESC";

    $qry_regis = $this->db_job->query($sql_regis);
    $callback = array(
      "status" => 200,
      "type" => TRUE,
      "msg" => "OK",
      "login" => $login,
      "last_login" => $qry_login->result_array()[0],
      "regis" =>$regis,
      "log_regis" =>$qry_regis->result_array()
    );
      return $callback;
  }
}
