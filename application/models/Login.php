<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Login extends CI_Model{

  function __construct()
  {
    parent::__construct();
    $this->db_job = $this->load->database('Job',TRUE);
    $this->load->model('Functions');
    $this->now = $this->Functions->date_time_get();
    $this->path = $this->Functions->get_path();
  }

  public function Login($email,$password)
  {
    $rowuser = $this->Users->getUserByEmail($email);
    if ($rowuser['status'] == 200 ) {
      if ($rowuser['data']['status'] == '1') {
        if ($rowuser['data']['password'] == $password) {
          $key = md5("ch1nn@p@T.J");
                $tokenData = array(
                        "company_id" => $rowuser["data"]["company_id"],
                        "uid" => $rowuser["data"]["uid"],
                        "fname" => $rowuser["data"]["fname"],
                        "lname" => $rowuser["data"]["lname"],
                        "email" => $rowuser["data"]["email"],
                        "level" => $rowuser["data"]["level"],
                        "status" => $rowuser["data"]["status"],
                        "iat" => time(),
                      );
                $jwt = JWT::encode($tokenData, $key);
          $callback = array(
                    "status" => 200,
                              "type" => TRUE,
                              "msg" => "OK",
                              "token" => $jwt,
                              "channel" => md5($rowuser["data"]["uid"]),
                   );
          $this->Functions->insertLog($rowuser["data"]["company_id"],$rowuser["data"]["uid"],'',"Login", "เข้าสู่ระบบ", "users","");

          return $callback;
        }
        else{
          $callback = array(
                    "status" => 301,
                              "type" => FALSE,
                              "msg" => "Password Incorrect",
                              "token" => "",
                   );
          $this->Functions->insertLog($rowuser["data"]["company_id"],$rowuser["data"]["uid"],'',"Password Incorrect", "รหัสผ่านผิด", "users","");

          return $callback;
        }
      }elseif ($rowuser['data']['status'] == '9') {
        $callback = array(
                          "status" => 302,
                          "type" => FALSE,
                          "msg" => "Wait Conform",
                        );
        $this->Functions->insertLog($rowuser["data"]["company_id"],$rowuser["data"]["uid"],'',"Wait Conform", "รอคอนเฟิร์ม", "users","");

        return $callback;
      }else{
        $callback = array(
                          "status" => 303,
                          "type" => FALSE,
                          "msg" => "User Disabled",
                        );
        $this->Functions->insertLog($rowuser["data"]["company_id"],$rowuser["data"]["uid"],'',"User Disabled", "ถูกปิดการใช้งาน", "users","");
        return $callback;
      }
    }else{
      $callback = array(
                        "status" => 300,
                        "type" => FALSE,
                        "msg" => "User Not Found",
                      );
      return $callback;
    }
  }

  public function Logout($company_id,$uid)
  {
    $callback = array(
                        "status" => 200,
                        "type" => TRUE,
                        "msg" => "OK",
                     );
    $this->Functions->insertLog($company_id,$uid,'',"Logout", "ออกจากระบบ", "users","");
    return $callback;
  }

  public function loginApplicant($uid,$citizen)
  {
    $rowuser = $this->Applicant->getApplicantByUid($uid);
     if ($rowuser['count'] > 0 ) {
    // if ($rowuser['status'] == 200 ) {
      $callback = array();
      // if ($rowuser['data']['AP_status'] == '1') {
        $key = md5("ch1nn@p@T.J");
              $tokenData = array(
                      "uid" => $rowuser["data"]['uid'],
                      "fname" => $rowuser["data"]['fname'],
                      "lname" => $rowuser["data"]["lname"],
                      "citizen" => $rowuser["data"]["citizen"],
                      "job_description_1" => $rowuser["data"]["job_description_code_1"],
                      "job_description_2" => $rowuser["data"]["job_description_code_2"],
                      "birth_date" => $rowuser["data"]["birth_date"],
                      "gender" => $rowuser["data"]["sex"],
                      "zone_id" => $rowuser["data"]["zone_id"],
                      "degree_code" => $rowuser["data"]["degree_code"],
                      "company_id" => '',
                      "iat" => time(),
                    );
              $jwt = JWT::encode($tokenData, $key);
        $callback = array(
                  "status" => 200,
                            "type" => TRUE,
                            "msg" => "OK",
                            "token" => $jwt,
                            "channel" => md5($rowuser["data"]["uid"]),
                 );
        $this->Functions->insertLog('',$rowuser["data"]["uid"],'',"Login", "เข้าสู่ระบบ", "Applicant","");
        return $callback;
      // }elseif ($rowuser['data']['AP_status'] == '9') {
      //   $callback = array(
      //             "status" => 201,
      //                       "type" => FALSE,
      //                       "msg" => "Wait Resume",
      //                       "citizen" => $rowuser["data"]["id_card"],
      //            );
      //   $this->Functions->insertLog('',$uid,'',"Wait Resume", "รอประวัติ", "Applicant");
      //   return $callback;
      // }
    }else {
      $key = md5("ch1nn@p@T.J");
            $tokenData = array(
                    "uid" => $rowuser["data"]['uid'],
                    "fname" => $rowuser["data"]['fname'],
                    "lname" => $rowuser["data"]["lname"],
                    "citizen" => $rowuser["data"]["id_card"],
                    "job_description_1" => '',
                    "job_description_2" => '',
                    "birth_date" => '',
                    "gender" => '',
                    "zone_id" => '',
                    "degree_code" => '',
                    "company_id" => '',
                    "iat" => time(),
                  );
            $jwt = JWT::encode($tokenData, $key);
      $callback = array(
                "status" => 200,
                          "type" => TRUE,
                          "msg" => "OK",
                          "token" => $jwt,
                          "channel" => md5($rowuser["data"]["uid"]),
               );
      $this->Functions->insertLog('',$rowuser["data"]["uid"],'',"Login", "เข้าสู่ระบบ", "Applicant","");
      return $callback;
    }
  }

  public function logoutApplicant($uid)
  {
    $callback = array(
                        "status" => 200,
                        "type" => TRUE,
                        "msg" => "OK",
                     );
    $this->Functions->insertLog('',$uid,'',"Logout", "ออกจากระบบ", "Applicant","");
    return $callback;
  }

}
