<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Company extends CI_Model{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Functions');
    $this->db_job = $this->load->database('Job',TRUE);
    $this->now = $this->Functions->date_time_get();
    $this->path = $this->Functions->get_path();

  }

  private function GenerateUID()
  {
    $query = "SELECT company_id FROM company ORDER BY company_id DESC LIMIT 1";
    $qryid = $this->db_job->query($query);
    if($qryid->num_rows()>0){
      $userId = $qryid->result_array();
      $oldId = $userId[0]["company_id"];
      $oldYMD =  substr($oldId,1,6);
      $y = sprintf("%02d",date('y'));
      $m = sprintf("%02d",date('m'));
      $d = sprintf("%02d",date('d'));
      $newYMD =  $y.$m.$d;
      $oldNUM =  sprintf("%06d",substr($oldId,7,6));
      if ($oldYMD == $newYMD) {
          $newNUM = $oldNUM+1;
          $newNUM = sprintf("%06d",$newNUM);
      }else{
          $newNUM = "000001";
      }
      return "C".$newYMD.$newNUM;
    }else{
      $y = sprintf("%02d",date('y'));
      $m = sprintf("%02d",date('m'));
      $d = sprintf("%02d",date('d'));
      $newYMD =  $y.$m.$d;
      $newNUM = "000001";
      return "C".$newYMD.$newNUM;
    }
  }

  public function create($company_name, $tin, $create_by)
  {
    $company_id = $this->GenerateUID();
    $package_expire = date('Y-m-d',strtotime($this->now . "+30 days"));
    $create_at = $this->now;
    $sql = "INSERT INTO company
            VALUES('$company_id', '$company_name', '$tin', '$this->now', '$create_by', '', '', '01', '$package_expire', '9','')";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "id" => $company_id
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }

  public function createDetail($company_id,$company_detail, $company_kind, $company_kind_detail, $company_address, $pro_id, $aumphur_id,$tumbon_id, $web_site, $company_tel, $travel,$uid)
  {
    $sql = "INSERT INTO company_detail
            VALUES('$company_id','$company_detail','$company_kind','$company_kind_detail','$company_address','$pro_id','$aumphur_id','$tumbon_id','$web_site','$company_tel', '$travel','')";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $this->Functions->insertLog($company_id,$uid,$company_id,"Manage Company Information", "จัดการข้อมูลบริษัท", "company","");
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "id" => $company_id
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }

  public function editCompany($company_id,$company_name, $tin,$update_by)
  {
    $sql = "UPDATE company
            SET company_name = '$company_name',
                tin = '$tin',
                update_by = '$update_by',
                update_at = '$this->now'
            WHERE company_id = '$company_id'";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }

  public function editDetail($company_id,$uid,$company_detail, $company_kind, $company_kind_detail, $company_address, $pro_id, $aumphur_id,$tumbon_id, $web_site, $company_tel, $travel)
  {
    $sql = "UPDATE company_detail
            SET company_detail = '$company_detail',
                company_kind = '$company_kind',
                company_kind_detail = '$company_kind_detail',
                company_address = '$company_address',
                pro_id = '$pro_id',
                aumphur_id = '$aumphur_id',
                tumbon_id = '$tumbon_id',
                web_site = '$web_site',
                company_tel = '$company_tel',
                travel = '$travel'
            WHERE company_id = '$company_id'";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $this->Functions->insertLog($company_id,$uid,$company_id,"Edit Company Information", "แก้ไขข้อมูลบริษัท", "company","");
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "id" =>$company_id
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }

  public function updateStatus($company_id,$status)
  {
    $sql = "UPDATE company
            SET company_status = '$status'
            WHERE company_id = '$company_id'";
    $qry  = $this->db_job->query($sql);
    if($qry){
                    $callback = array(
                      "status" => 200,
                      "type" => TRUE,
                      "msg" => "OK",
                    );
     }else{
                      $callback = array(
                        "status" => 400,
                        "type" => FALSE,
                        "msg" => "Insert Failed",
                      );
    }
    return $callback;
  }

  public function getcompnyById($company_id)
  {
    $sql = "SELECT * FROM company WHERE company_id = '$company_id'";
    $qry  = $this->db_job->query($sql);
    return $qry->result_array();
  }

  public function getDeail($company_id)
  {
    $sql = "SELECT * FROM company_detail WHERE company_id = '$company_id'";
    $qry  = $this->db_job->query($sql);
    if($qry){
      if ($qry->num_rows() > 0) {
        $data = $qry->result_array()[0];
        $data['map'] = $this->path."assets/documents/Company_detail/".$data['map'];
        if ($data['pro_id'] != '') {
          $data['pro_id'] = $this->Master->provinceById($data['pro_id'])['data'][0];
        }
        if ($data['aumphur_id'] != '') {
          $data['aumphur_id'] = $this->Master->aumphurById($data['pro_id']['pro_id'],$data['aumphur_id'])['data'][0];
        }
        if ($data['tumbon_id'] != '') {
          $data['tumbon_id'] = $this->Master->tumbonById($data['pro_id']['pro_id'],$data['aumphur_id']['district_id'],$data['tumbon_id'])['data'][0];
        }
        if ($data['company_kind'] != '') {
          $data['company_kind'] = $this->Master->getcompany_kindById($data['company_kind'])['data'][0];
        }
        $data['company']= $this->getcompnyById($company_id)[0];
        $data['company']['image'] = $this->path."assets/documents/Company_logo/".$data['company']['image'];

      }else {
        $data = $qry->result_array();
      }
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "count" => $qry->num_rows(),
        "data" => $data
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }

  public function file_company($id,$file)
  {
    $imgname = $file["name"][0];
    $tmp = $file["tmp_name"][0];
    $image_array = explode(".",$imgname);
    $type = $image_array[count($image_array)-1];
    if ($type!="png" || $type!="jpg" || $type!="jpeg" || $type!="gif") {
      $newname = $id."_".time().".".$type;
      if (move_uploaded_file($tmp,"assets/documents/Company_logo/".$newname)){
        $sql = "UPDATE company SET image = '$newname' WHERE company_id ='$id' ";
        $qry = $this->db_job->query($sql);
        if ($qry) {
          $callback = array(
            "status" => 200,
            "type" => TRUE,
            "msg" => "OK",
          );
        }else {
          $callback = array(
            "status" => 400,
            "type" => FALSE,
            "msg" => "Insert Failed",
          );
        }
      }else {
        $callback = array(
          "status" => 400,
          "type" => FALSE,
          "msg" => "Upload Failed",
        );
      }
    }else {
      $callback = array(
        "status" => 406,
        "type" => FALSE,
        "msg" => "Type Not Support",
      );
    }
    return $callback;
  }

  public function file_company_detail($id,$file)
  {
    $imgname = $file["name"][0];
    $tmp = $file["tmp_name"][0];
    $image_array = explode(".",$imgname);
    $type = $image_array[count($image_array)-1];
    if ($type!="png" || $type!="jpg" || $type!="jpeg" || $type!="gif" ) {
      $newname = $id."_".time().".".$type;
      if (move_uploaded_file($tmp,"assets/documents/Company_detail/".$newname)){
        $sql = "UPDATE company_detail SET map = '$newname' WHERE company_id ='$id' ";
        $qry = $this->db_job->query($sql);
        if ($qry) {
          $callback = array(
            "status" => 200,
            "type" => TRUE,
            "msg" => "OK",
          );
        }else {
          $callback = array(
            "status" => 400,
            "type" => FALSE,
            "msg" => "Insert Failed",
          );
        }
      }else {
        $callback = array(
          "status" => 400,
          "type" => FALSE,
          "msg" => "Upload Failed",
        );
      }
    }else {
      $callback = array(
        "status" => 406,
        "type" => FALSE,
        "msg" => "Type Not Support",
      );
    }
    return $callback;
  }

  public function getPackageByCompany($company_id)
  {
    $sql_package = "SELECT package_id FROM company WHERE company_id = '$company_id'";
    $qry_package  = $this->db_job->query($sql_package);
    $package_id = $qry_package->result_array()[0]['package_id'];
    $sql = "SELECT * FROM package WHERE package_id = '$package_id'";
    $qry  = $this->db_job->query($sql);
    return $qry->result_array()[0];
  }



}
