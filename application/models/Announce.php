<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Announce extends CI_Model{

  function __construct()
  {
    parent::__construct();
    $this->db_job = $this->load->database('Job',TRUE);
    $this->db_recruitment= $this->load->database('Recruitment',TRUE);
    // $this->db_srv= $this->load->database('sqlsrv',TRUE);
    $this->now = $this->Functions->date_time_get();
    $this->path = $this->Functions->get_path();
    $this->load->model('Applicant');

  }

  private function GenerateAID()
  {
    $query = "SELECT announce_id FROM announce ORDER BY announce_id DESC LIMIT 1";
    $qryid = $this->db_job->query($query);
    if($qryid->num_rows()>0){
      $userId = $qryid->result_array();
      $oldId = $userId[0]["announce_id"];
      $oldYMD =  substr($oldId,1,6);
      $y = sprintf("%02d",date('y'));
      $m = sprintf("%02d",date('m'));
      $d = sprintf("%02d",date('d'));
      $newYMD =  $y.$m.$d;
      $oldNUM =  sprintf("%06d",substr($oldId,7,6));
      if ($oldYMD == $newYMD) {
          $newNUM = $oldNUM+1;
          $newNUM = sprintf("%06d",$newNUM);
      }else{
          $newNUM = "000001";
      }
      return "A".$newYMD.$newNUM;
    }else{
      $y = sprintf("%02d",date('y'));
      $m = sprintf("%02d",date('m'));
      $d = sprintf("%02d",date('d'));
      $newYMD =  $y.$m.$d;
      $newNUM = "000001";
      return "A".$newYMD.$newNUM;
    }
  }

  public function add($image, $announce_title, $income_min,
   $income_max, $type_job, $working_start_date, $working_end_date, $working_day, $working_start, $working_end,
   $benefits, $detail, $property, $experience, $education, $age_min,
   $age_max, $gender, $license, $zone, $zone_detail, $remark, $company_id,
   $create_by,$job_description_1,$job_description_2,$staff_id)
  {
    $announce_id = $this->GenerateAID();
    $sql = "INSERT INTO announce
            VALUES(
              '$announce_id', '$image', '$announce_title', '$income_min','$income_max',
              '$type_job','$working_start_date', '$working_end_date', '$working_day', '$working_start', '$working_end',
              '$benefits', '$detail', '$property', '$experience', $education, $age_min,
               $age_max, '$gender', '$license', '$zone', '$zone_detail', '$remark', '$company_id',
              '$this->now', '$create_by', '$this->now', '$create_by', '0','0','0','1','$job_description_1','$job_description_2','$staff_id')";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $this->Functions->insertLog($company_id,$create_by,$announce_id,"Add Announce", "เพิ่มประกาศ", "announce","");
      $callback = array(
       "status" => 200,
       "type" => TRUE,
       "msg" => "OK",
       "id" => $announce_id
      );
    }
    else{
     $callback = array(
       "status" => 201,
       "type" => FALSE,
       "msg" => "Query Error",
       "data" => $sql
      );
    }
    return $callback;

  }

  public function edit($announce_id,$image, $announce_title, $income_min,
   $income_max, $type_job, $working_start_date, $working_end_date,
   $working_day, $working_start, $working_end,
   $benefits, $detail, $property, $experience, $education, $age_min,
   $age_max, $gender, $license, $zone, $zone_detail, $remark,
   $job_description_1,$job_description_2,
  $update_by,$company_id,$staff_id)
  {
    $sql="UPDATE announce
          SET announce_title = '$announce_title',
              income_min = '$income_min',
              income_max = '$income_max',
              type_job = '$type_job',
              work_start_date = '$working_start_date',
              work_end_date = '$working_end_date',
              working_day = '$working_day',
              working_start = '$working_start',
              working_end = '$working_end',
              benefits = '$benefits',
              detail = '$detail',
              property = '$property',
              experience = '$experience',
              education = '$education',
              age_min = '$age_min',
              age_max = '$age_max',
              gender = '$gender',
              license = '$license',
              zone = '$zone',
              zone_detail = '$zone_detail',
              remark = '$remark',
              update_by = '$update_by',
              update_at = '$this->now'
          WHERE announce_id = '$announce_id'";
          $qry  = $this->db_job->query($sql);
          if($qry){
            $this->Functions->insertLog($company_id,$update_by,$announce_id,"Edit Announce", "แก้ไขประกาศ", "announce","");
            $callback = array(
              "status" => 200,
              "type" => TRUE,
              "msg" => "OK",
            );
          }else{
            $callback = array(
              "status" => 201,
              "type" => FALSE,
              "msg" => "Query Error",
              "data" => $sql
            );
          }
          return $callback;
  }

  public function updateImg($announce_id,$image)
  {
    $sql="UPDATE announce
          SET image = '$image'
          WHERE announce_id = '$announce_id'";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }

  public function updateStatus($announce_id,$status,$company_id,$uid)
  {
    $sql="UPDATE announce
          SET status = '$status'
          WHERE announce_id = '$announce_id'";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $this->Functions->insertLog($company_id,$uid,$announce_id,"Update Status Announce", "แก้ไขสถานประกาศ", "Announce","");
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }

  public function file_announce($id,$file)
  {
    $imgname = $file["name"][0];
    $tmp = $file["tmp_name"][0];
    $image_array = explode(".",$imgname);
    $type = $image_array[count($image_array)-1];
    if ($type!="png" || $type!="jpg" || $type!="jpeg" || $type!="gif" || $type!="pdf" ) {
      $newname = $id."_".time().".".$type;
      if (move_uploaded_file($tmp,"assets/documents/Announce/".$newname)){
        $sql = "UPDATE announce SET image = '$newname' WHERE announce_id ='$id' ";
        $qry = $this->db_job->query($sql);
        if ($qry) {
          $callback = array(
            "status" => 200,
            "type" => TRUE,
            "msg" => "OK",
          );
        }else {
          $callback = array(
            "status" => 400,
            "type" => FALSE,
            "msg" => "Insert Failed",
          );
        }
      }else {
        $callback = array(
          "status" => 400,
          "type" => FALSE,
          "msg" => "Upload Failed",
        );
      }
    }else {
      $callback = array(
        "status" => 406,
        "type" => FALSE,
        "msg" => "Type Not Support",
      );
    }
    return $callback;
  }

  public function getannounceByCreate($company_id,$uid)
  {
    $sql = "SELECT * FROM announce
            LEFT JOIN company ON announce.company_id = company.company_id
            LEFT JOIN company_detail  ON announce.company_id = company_detail.company_id
            WHERE status = '1'";

    if ($company_id != "" && $uid != "") {
      $sql .= " AND announce.company_id = '$company_id' AND announce.create_by = '$uid'";
    }elseif ($company_id != "" && $uid == "") {
      $sql .= " AND announce.company_id = '$company_id'";
    }elseif ($company_id == "" && $uid != "") {
      $sql .= " AND announce.create_by = '$uid'";
    }
    $sql .= "ORDER BY announce.update_at DESC";
    $qry = $this->db_job->query($sql);
    $dataset = $qry->result_array();
    for ($i=0; $i < $qry->num_rows(); $i++) {
      $dataset[$i]["percent"] = "";
      $dataset[$i]["company"] = $this->Company->getDeail($dataset[$i]['company_id'])['data'];
      // $dataset[$i]["job_description_2"] = $this->Master->getjob_description_2ById($dataset[$i]['job_description_1'],$dataset[$i]['job_description_2']);
      $dataset[$i]["job_description_1"] = $this->Master->getPositionById($dataset[$i]['job_description_1']);
      $dataset[$i]["education"] = $this->Master->geteducationById($dataset[$i]['education']);
      $dataset[$i]["gender"] = $this->Master->getGenderById($dataset[$i]['gender']);
      $dataset[$i]["zone_detail"] = $this->Master->aumphurById($dataset[$i]['zone'],$dataset[$i]['zone_detail']);
      $dataset[$i]["zone"] = $this->Master->provinceById($dataset[$i]['zone']);

    }


    if ($qry) {
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "count" => $qry->num_rows(),
        "data" => $dataset
      );
    }else {
      $callback = array(
        "status" => 400,
        "type" => FALSE,
        "msg" => "Insert Failed",
      );
    }
    return $callback;
  }
  public function getannounceAll($uid)
  {
    $sql = "SELECT announce.*
            FROM announce
            LEFT JOIN company ON announce.company_id = company.company_id
            LEFT JOIN company_detail  ON announce.company_id = company_detail.company_id
            WHERE announce.status = '1' AND company.company_status = '1' ORDER BY update_at DESC ";
    $qry = $this->db_job->query($sql);
    $dataset = $qry->result_array();
    for ($i=0; $i < $qry->num_rows(); $i++) {
      $dataset[$i]["percent"] = "";
      $dataset[$i]["company"] = $this->Company->getDeail($dataset[$i]['company_id'])['data'];
      // $dataset[$i]["job_description_2"] = $this->Master->getjob_description_2ById($dataset[$i]['job_description_1'],$dataset[$i]['job_description_2']);
      $dataset[$i]["job_description_1"] = $this->Master->getPositionById($dataset[$i]['job_description_1']);
      $dataset[$i]["education"] = $this->Master->geteducationById($dataset[$i]['education']);
      $dataset[$i]["gender"] = $this->Master->getGenderById($dataset[$i]['gender']);
      $dataset[$i]["zone_detail"] = $this->Master->aumphurById($dataset[$i]['zone'],$dataset[$i]['zone_detail']);
      $dataset[$i]["zone"] = $this->Master->provinceById($dataset[$i]['zone']);
      if ($uid <>"") {
        $dataset[$i]["favorite_job"] = $this->chk_favorite_me($uid,$dataset[$i]['announce_id']);
      }else {
        $dataset[$i]["favorite_job"] = FALSE;
      }
    }
    if ($qry) {
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "count" =>$qry->num_rows(),
        "data" => $dataset,
      );
    }else {
      $callback = array(
        "status" => 400,
        "type" => FALSE,
        "msg" => "Insert Failed",
      );
    }
    return $callback;
  }
  public function getAnnounce_content($uid)
  {
    // $sql = "SELECT announce.*
    //         FROM announce
    //         LEFT JOIN company ON announce.company_id = company.company_id
    //         LEFT JOIN company_detail  ON announce.company_id = company_detail.company_id
    //         WHERE announce.status = '1' AND company.company_status = '1' ORDER BY update_at DESC ";
    if ($uid == "") {
      $sql = "SELECT announce.*
              FROM announce
              LEFT JOIN company ON announce.company_id = company.company_id
              LEFT JOIN company_detail  ON announce.company_id = company_detail.company_id
              WHERE announce.status = '1' AND company.company_status = '1' ORDER BY update_at DESC ";
    }else {
      $sql_last="SELECT announce.job_description_1,announce.job_description_2
              FROM announce
              LEFT JOIN company ON announce.company_id = company.company_id
              LEFT JOIN company_detail  ON announce.company_id = company_detail.company_id
              WHERE announce_id  = (SELECT passive FROM db_job.log_system where uid = '$uid' AND passive <>'' order by log_date desc limit 1)";
      $qry_last = $this->db_job->query($sql_last);
      if ($qry_last->num_rows() >0) {
        $job_description_1= $qry_last->result_array()[0]['job_description_1'];
        $job_description_2= "";
        if ($job_description_1 <> "") {
          $job_description_1 = "AND job_description_1 = '$job_description_1'";
        }
        $sql = "SELECT announce.*
                FROM announce
                LEFT JOIN company ON announce.company_id = company.company_id
                LEFT JOIN company_detail  ON announce.company_id = company_detail.company_id
                WHERE announce.status = '1' AND company.company_status = '1' $job_description_1 $job_description_2 ORDER BY update_at DESC ";
      }else {
        $sql = "SELECT announce.*
                FROM announce
                LEFT JOIN company ON announce.company_id = company.company_id
                LEFT JOIN company_detail  ON announce.company_id = company_detail.company_id
                WHERE announce.status = '1' AND company.company_status = '1' ORDER BY update_at DESC ";
      }
    }
    $qry = $this->db_job->query($sql);
    $dataset = $qry->result_array();
    for ($i=0; $i < $qry->num_rows(); $i++) {
      $dataset[$i]["percent"] = "";
      $dataset[$i]["company"] = $this->Company->getDeail($dataset[$i]['company_id'])['data'];
      // $dataset[$i]["job_description_2"] = $this->Master->getjob_description_2ById($dataset[$i]['job_description_1'],$dataset[$i]['job_description_2']);
      $dataset[$i]["job_description_1"] = $this->Master->getPositionById($dataset[$i]['job_description_1']);
      $dataset[$i]["education"] = $this->Master->geteducationById($dataset[$i]['education']);
      $dataset[$i]["gender"] = $this->Master->getGenderById($dataset[$i]['gender']);
      $dataset[$i]["zone_detail"] = $this->Master->aumphurById($dataset[$i]['zone'],$dataset[$i]['zone_detail']);
      $dataset[$i]["zone"] = $this->Master->provinceById($dataset[$i]['zone']);
    }
    if ($qry) {
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "count" =>$qry->num_rows(),
        "data" => $dataset,
      );
    }else {
      $callback = array(
        "status" => 400,
        "type" => FALSE,
        "msg" => "Insert Failed",
      );
    }
    return $callback;
  }
  public function getannounce_search($uid,$search_job_position,$search_job_description_2,$search_type_job,$search_zone,$search_zone_detail,$search_education,$search_companyname,$offset)
  {
    $sql = "SELECT announce.*
            FROM announce
            LEFT JOIN company ON announce.company_id = company.company_id
            LEFT JOIN company_detail  ON announce.company_id = company_detail.company_id
            WHERE announce.status = '1' AND company.company_status = '1' ";
    if ($search_job_position != '') {
      $sql .= "AND job_description_1 = '$search_job_position'";
    }
    if ($search_job_description_2 != '') {
      $sql .= "AND job_description_2 = '$search_job_description_2'";
    }
    if ($search_type_job != '') {
      $sql .= "AND type_job = '$search_type_job'";
    }
    if ($search_zone != '') {
      $sql .= "AND zone = '$search_zone'";
    }
    if ($search_zone_detail != '') {
      $sql .= "AND zone_detail = '$search_zone_detail'";
    }
    if ($search_education != '') {
      $sql .= "AND education = '$search_education'";
    }
    if ($search_companyname != '') {
      $sql .= "AND company_name LIKE '%$search_companyname%'";
    }
    $sql .= "ORDER BY create_at DESC  LIMIT 9999 OFFSET $offset";
    $qry = $this->db_job->query($sql);
    $dataset = $qry->result_array();
    for ($i=0; $i < $qry->num_rows(); $i++) {
      $dataset[$i]["percent"] = "";
      $dataset[$i]["company"] = $this->Company->getDeail($dataset[$i]['company_id'])['data'];
      // $dataset[$i]["job_description_2"] = $this->Master->getjob_description_2ById($dataset[$i]['job_description_1'],$dataset[$i]['job_description_2']);
      $dataset[$i]["job_description_1"] = $this->Master->getPositionById($dataset[$i]['job_description_1']);
      $dataset[$i]["education"] = $this->Master->geteducationById($dataset[$i]['education']);
      $dataset[$i]["gender"] = $this->Master->getGenderById($dataset[$i]['gender']);
      $dataset[$i]["zone_detail"] = $this->Master->aumphurById($dataset[$i]['zone'],$dataset[$i]['zone_detail']);
      $dataset[$i]["zone"] = $this->Master->provinceById($dataset[$i]['zone']);

      if ($uid <>"") {
        $dataset[$i]["favorite_job"] = $this->chk_favorite_me($uid,$dataset[$i]['announce_id']);
      }else {
        $dataset[$i]["favorite_job"] = FALSE;
      }
    }
    if ($qry) {
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "count" =>$qry->num_rows(),
        "data" => $dataset,
        "sql" => $sql
      );
    }else {
      $callback = array(
        "status" => 400,
        "type" => FALSE,
        "msg" => "Insert Failed",
      );
    }
    return $callback;
  }
  public function getannounce_topview($uid)
  {
    $sql = "SELECT announce.*
            FROM announce
            LEFT JOIN company ON announce.company_id = company.company_id
            LEFT JOIN company_detail  ON announce.company_id = company_detail.company_id
            WHERE announce.status = '1' AND company.company_status = '1' ORDER BY count_view DESC limit 20";
    $qry = $this->db_job->query($sql);
    $dataset = $qry->result_array();
    for ($i=0; $i < $qry->num_rows(); $i++) {
      $dataset[$i]["percent"] = "";
      $dataset[$i]["company"] = $this->Company->getDeail($dataset[$i]['company_id'])['data'];
      // $dataset[$i]["job_description_2"] = $this->Master->getjob_description_2ById($dataset[$i]['job_description_1'],$dataset[$i]['job_description_2']);
      $dataset[$i]["job_description_1"] = $this->Master->getPositionById($dataset[$i]['job_description_1']);
      $dataset[$i]["education"] = $this->Master->geteducationById($dataset[$i]['education']);
      $dataset[$i]["gender"] = $this->Master->getGenderById($dataset[$i]['gender']);
      $dataset[$i]["zone_detail"] = $this->Master->aumphurById($dataset[$i]['zone'],$dataset[$i]['zone_detail']);
      $dataset[$i]["zone"] = $this->Master->provinceById($dataset[$i]['zone']);

      if ($uid <>"") {
        $dataset[$i]["favorite_job"] = $this->chk_favorite_me($uid,$dataset[$i]['announce_id']);
      }else {
        $dataset[$i]["favorite_job"] = FALSE;
      }
    }
    if ($qry) {
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "count" =>$qry->num_rows(),
        "data" => $dataset,
      );
    }else {
      $callback = array(
        "status" => 400,
        "type" => FALSE,
        "msg" => "Insert Failed",
      );
    }
    return $callback;
  }
  public function getannounce_topfavorite($uid)
  {
    $sql = "SELECT announce.*
            FROM announce
            LEFT JOIN company ON announce.company_id = company.company_id
            LEFT JOIN company_detail  ON announce.company_id = company_detail.company_id
            WHERE announce.status = '1' AND company.company_status = '1' ORDER BY count_favorite DESC limit 20";
    $qry = $this->db_job->query($sql);
    $dataset = $qry->result_array();
    for ($i=0; $i < $qry->num_rows(); $i++) {
      $dataset[$i]["percent"] = "";
      $dataset[$i]["company"] = $this->Company->getDeail($dataset[$i]['company_id'])['data'];
      // $dataset[$i]["job_description_2"] = $this->Master->getjob_description_2ById($dataset[$i]['job_description_1'],$dataset[$i]['job_description_2']);
      $dataset[$i]["job_description_1"] = $this->Master->getPositionById($dataset[$i]['job_description_1']);
      $dataset[$i]["education"] = $this->Master->geteducationById($dataset[$i]['education']);
      $dataset[$i]["gender"] = $this->Master->getGenderById($dataset[$i]['gender']);
      $dataset[$i]["zone_detail"] = $this->Master->aumphurById($dataset[$i]['zone'],$dataset[$i]['zone_detail']);
      $dataset[$i]["zone"] = $this->Master->provinceById($dataset[$i]['zone']);

      if ($uid <>"") {
        $dataset[$i]["favorite_job"] = $this->chk_favorite_me($uid,$dataset[$i]['announce_id']);
      }else {
        $dataset[$i]["favorite_job"] = FALSE;
      }
    }
    if ($qry) {
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "count" =>$qry->num_rows(),
        "data" => $dataset,
      );
    }else {
      $callback = array(
        "status" => 400,
        "type" => FALSE,
        "msg" => "Insert Failed",
      );
    }
    return $callback;
  }
  public function getannounce_topincome($uid)
  {
    $sql = "SELECT announce.*
            FROM announce
            LEFT JOIN company ON announce.company_id = company.company_id
            LEFT JOIN company_detail  ON announce.company_id = company_detail.company_id
            WHERE announce.status = '1' AND company.company_status = '1' ORDER BY income_max DESC limit 20";
    $qry = $this->db_job->query($sql);
    $dataset = $qry->result_array();
    for ($i=0; $i < $qry->num_rows(); $i++) {
      $dataset[$i]["percent"] = "";
      $dataset[$i]["company"] = $this->Company->getDeail($dataset[$i]['company_id'])['data'];
      // $dataset[$i]["job_description_2"] = $this->Master->getjob_description_2ById($dataset[$i]['job_description_1'],$dataset[$i]['job_description_2']);
      $dataset[$i]["job_description_1"] = $this->Master->getPositionById($dataset[$i]['job_description_1']);
      $dataset[$i]["education"] = $this->Master->geteducationById($dataset[$i]['education']);
      $dataset[$i]["gender"] = $this->Master->getGenderById($dataset[$i]['gender']);
      $dataset[$i]["zone_detail"] = $this->Master->aumphurById($dataset[$i]['zone'],$dataset[$i]['zone_detail']);
      $dataset[$i]["zone"] = $this->Master->provinceById($dataset[$i]['zone']);

      if ($uid <>"") {
        $dataset[$i]["favorite_job"] = $this->chk_favorite_me($uid,$dataset[$i]['announce_id']);
      }else {
        $dataset[$i]["favorite_job"] = FALSE;
      }
    }
    if ($qry) {
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "count" =>$qry->num_rows(),
        "data" => $dataset,
      );
    }else {
      $callback = array(
        "status" => 400,
        "type" => FALSE,
        "msg" => "Insert Failed",
      );
    }
    return $callback;
  }
  public function getannounce_myfavorite($uid)
  {
    $sql_favorite = "SELECT * FROM favorite_job where uid = '$uid' and status = '1'";
    $qry_favorite = $this->db_job->query($sql_favorite);
    $favorite = $qry_favorite->result_array();
    if($qry_favorite->num_rows() > 0){
      $sql = "SELECT announce.*
              FROM announce
              LEFT JOIN company ON announce.company_id = company.company_id
              LEFT JOIN company_detail  ON announce.company_id = company_detail.company_id
              WHERE announce_id IN (SELECT announce_id FROM favorite_job where uid = '$uid' and status = '1')";
              $qry = $this->db_job->query($sql);
              $dataset = $qry->result_array();
              for ($i=0; $i < $qry->num_rows(); $i++) {
                $dataset[$i]["percent"] = "";
                $dataset[$i]["company"] = $this->Company->getDeail($dataset[$i]['company_id'])['data'];
                // $dataset[$i]["job_description_2"] = $this->Master->getjob_description_2ById($dataset[$i]['job_description_1'],$dataset[$i]['job_description_2']);
                $dataset[$i]["job_description_1"] = $this->Master->getPositionById($dataset[$i]['job_description_1']);
                $dataset[$i]["education"] = $this->Master->geteducationById($dataset[$i]['education']);
                $dataset[$i]["gender"] = $this->Master->getGenderById($dataset[$i]['gender']);
                $dataset[$i]["zone_detail"] = $this->Master->aumphurById($dataset[$i]['zone'],$dataset[$i]['zone_detail']);
                $dataset[$i]["zone"] = $this->Master->provinceById($dataset[$i]['zone']);
                if ($uid <>"") {
                  $dataset[$i]["favorite_job"] = $this->chk_favorite_me($uid,$dataset[$i]['announce_id']);
                }else {
                  $dataset[$i]["favorite_job"] = FALSE;
                }
              }
              $callback = array(
                  "status" => 200,
                  "type" => TRUE,
                  "msg" => "OK",
                  "count" =>$qry->num_rows(),
                  "data" => $dataset,
              );
    }else{
      $callback = array(
        "status" => 202,
        "type" => FALSE,
        "msg" => "Query Error",
        "count" =>$qry->num_rows(),
        "data" => $sql
      );
    }
    return $callback;
  }
  public function getannounce_month($uid)
  {
    $month = date('m');
    $year = date('Y');
    $sql = "SELECT announce.*
            FROM announce
            LEFT JOIN company ON announce.company_id = company.company_id
            LEFT JOIN company_detail  ON announce.company_id = company_detail.company_id
            WHERE announce.status = '1' AND company.company_status = '1' AND MONTH(announce.update_at) = '$month'  and YEAR(announce.update_at) = '$year'";
    $qry = $this->db_job->query($sql);
    $dataset = $qry->result_array();
    for ($i=0; $i < $qry->num_rows(); $i++) {
      $dataset[$i]["percent"] = "";
      $dataset[$i]["company"] = $this->Company->getDeail($dataset[$i]['company_id'])['data'];
      // $dataset[$i]["job_description_2"] = $this->Master->getjob_description_2ById($dataset[$i]['job_description_1'],$dataset[$i]['job_description_2']);
      $dataset[$i]["job_description_1"] = $this->Master->getPositionById($dataset[$i]['job_description_1']);
      $dataset[$i]["education"] = $this->Master->geteducationById($dataset[$i]['education']);
      $dataset[$i]["gender"] = $this->Master->getGenderById($dataset[$i]['gender']);
      $dataset[$i]["zone_detail"] = $this->Master->aumphurById($dataset[$i]['zone'],$dataset[$i]['zone_detail']);
      $dataset[$i]["zone"] = $this->Master->provinceById($dataset[$i]['zone']);

      if ($uid <>"") {
        $dataset[$i]["favorite_job"] = $this->chk_favorite_me($uid,$dataset[$i]['announce_id']);
      }else {
        $dataset[$i]["favorite_job"] = FALSE;
      }
    }
    if($qry->num_rows() > 0){
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "count" => $qry->num_rows(),
        "data" => $dataset
      );
    }else{
      $callback = array(
        "status" => 202,
        "type" => FALSE,
        "msg" => "Query Error",
        "count" => $qry->num_rows(),
        "data" => $sql
      );
    }
    return $callback;
  }
  public function chk_apply($citizen,$announce_id)
  {
    $sql = "SELECT * FROM apply_job WHERE announce_id = '$announce_id' AND citizen_id = '$citizen'";
    $qry=$this->db_job->query($sql);
    if($qry->num_rows() <= 0){
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
      );
    }else{
      $callback = array(
        "status" => 202,
        "type" => FALSE,
        "msg" => "Query Duplicate",
        "data" => $sql,
        "count" => $qry->num_rows()
      );
    }
    return $callback;
  }
  public function apply_job($citizen,$uid,$announce_id)
  {
    $ckh = $this->chk_apply($citizen,$announce_id);
    if ($ckh['status'] == '202') {
      $no = $ckh['count']+1;
      $sql = "INSERT INTO apply_job VALUES('$announce_id','$no','$uid','$citizen','$this->now','1')";
    }else {
      $sql = "INSERT INTO apply_job VALUES('$announce_id','1','$uid','$citizen','$this->now','1')";
    }
    $qry=$this->db_job->query($sql);
    if($qry){
      $this->countApply($announce_id);
      $this->Functions->insertLog('',$uid,$announce_id,"Apply Job", "สนใจงาน", "Applicant","");
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;

  }
  public function chk_iRecruit($citizen)
  {
    $sql = "SELECT * FROM ir_register WHERE id_card = '$citizen' AND status in ('R' ,'W') AND DATEDIFF(now(), reg_date) < 14";
    $qry = $this->db_recruitment->query($sql);
    if ($qry->num_rows() > 0 ) {
      $register = $qry->result_array()[0];
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "Update",
        "reg_id" => $register['reg_id']
      );
    }else {
      $callback = array(
        "status" => 202,
        "type" => FALSE,
        "msg" => "Insert",
      );
    }
    return $callback;
  }

  private function GenerateRID()
  {
    $year = date("y")+43;
    $month = date("m");
    $now = $year.$month;
    $sql = "SELECT max(reg_id) as reg_id FROM ir_register";
    $qry = $this->db_recruitment->query($sql);
    $run = $qry->result_array()[0]['reg_id'];
        $sub_str = substr($run,-4);
        $sub_date = substr($run,4,4);
          if($sub_date == $now){
            $id = $sub_str+1;
          }else{
            $id = 1;
          }
     return $run_no = "RMR-".$now.sprintf("%04d",$id); //T = temporary ชั่วคราว
  }
  public function register_iRecruit($citizen,$pfix,$sex,$fname,$lname,$birth_date,$mobile,$degree,
  $experience, $staff_id, $infor_id,$position,$province,$aumphur_id)
  {
    $reg_id = $this->GenerateRID();
    $status_add = "1";
    $sql_chkStaff = "SELECT * FROM hr_staff WHERE staff_id = '$citizen'";
    $qry_chkStaff = $this->db_recruitment->query($sql_chkStaff);
    if ($qry_chkStaff->num_rows() > 0) {
      $status_add = "3";
    }
    $sql_chkRegis = "SELECT * FROM ir_register WHERE id_card = '$citizen'";
    $qry_chkRegis = $this->db_recruitment->query($sql_chkRegis);
    if ($qry_chkRegis->num_rows()) {
      $status_add = "2";
    }
    $age = "";
    $sql="INSERT INTO ir_register
            VALUES('$reg_id','$citizen','$pfix','$sex','$fname','$lname','$birth_date','$age','$mobile','$degree','$experience','$status_add',
            '$this->now','$staff_id', '$infor_id','R',null,null,null,null,null,null,'W','$province','$aumphur_id')";
    $qry = $this->db_recruitment->query($sql);
    if ($qry) {
      $this->insert_position_iRecruit($reg_id,$position);
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "data" => $reg_id,
      );
    }else {
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }

  public function chk_position_iRecruit($reg_id,$position)
  {
    $sql="SELECT * FROM ir_register_position WHERE reg_id = '$reg_id' AND position = '$position'";
    $qry = $this->db_recruitment->query($sql);
    if ($qry->num_rows() > 0 ) {
      $callback = array(
        "status" => 202,
        "type" => FALSE,
        "msg" => "Query Duplicate",
        "data" => $sql,
        "position" =>$position
      );
    }else {
      $callback = $this->insert_position_iRecruit($reg_id,$position);
    }
    return $callback;
  }

  public function insert_position_iRecruit($reg_id,$position)
  {
    $sql="INSERT INTO ir_register_position
            VALUES('$reg_id','$position','$this->now')";
    $qry = $this->db_recruitment->query($sql);
    if ($qry) {
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "data" => $reg_id,
        "position" =>$position
      );
    }else {
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }

  public function chk_announce_iRecruit($reg_id,$announce_id)
  {
    $sql="SELECT * FROM ir_register_announce WHERE reg_id = '$reg_id' AND announce_id = '$announce_id'";
    $qry = $this->db_recruitment->query($sql);
    if ($qry->num_rows() > 0 ) {
      $callback = array(
        "status" => 202,
        "type" => FALSE,
        "msg" => "Query Duplicate",
        "data" => $sql
      );
    }else {
      $callback = $this->insert_announce_iRecruit($reg_id,$announce_id);
    }
    return $callback;
  }

  public function insert_announce_iRecruit($reg_id,$announce_id)
  {
    $announce = $this->getAnnounceById($announce_id)['data'][0];
    $announce_title = $announce['announce_title'];
    $income_min = $announce['income_min'];
    $income_max = $announce['income_max'];
    $type_job = $announce['type_job'][0]['name'];
    $work_start_date = $announce['work_start_date'];
    $work_end_date = $announce['work_end_date'];
    $working_day=$announce['working_day'][0]['name'];
    $working_start=$announce['working_start'];
    $working_end=$announce['working_end'];
    $benefits=$announce['benefits'];
    $detail=$announce['detail'];
    $property=$announce['property'];
    $experience=$announce['experience'];
    $education=$announce['education'][0]['name'];
    $age_min=$announce['age_min'];
    $age_max=$announce['age_max'];
    $gender=$announce['gender'][0]['name'];
    $license=$announce['license'][0]['driving_license_type_code'];
    $zone=$announce['zone']['data'][0]['pro_thname'];
    $zone_detail=$announce['zone_detail']['data'][0]['district_thlist'];
    $remark=$announce['remark'];
    $company_id=$announce['company']['company']['company_name'];
    $job_description_1=$announce['job_description_1'][0]['position_name'];
    $staff_id=$announce['staff_id'];
    $sql = "INSERT INTO ir_register_announce
            VALUES('$reg_id', '$announce_id', '$announce_title', '$income_min', '$income_max', '$type_job', '$work_start_date',
              '$work_end_date', '$working_day', '$working_start', '$working_end', '$benefits', '$detail', '$property', '$experience',
              '$education', '$age_min', '$age_max', '$gender', '$license', '$zone', '$zone_detail', '$remark', '$company_id',
               '$job_description_1','', '$staff_id')";

    $qry = $this->db_recruitment->query($sql);
    if ($qry) {
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
      );
    }else {
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }

  public function chk_favorite($uid,$announce_id)
  {
    $sql = "SELECT * FROM favorite_job WHERE announce_id = '$announce_id' AND uid = '$uid'";
    $qry=$this->db_job->query($sql);
    if($qry->num_rows() <= 0){
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
      );
    }else{
      if ($qry->result_array()[0]['status'] == 0) {
        $callback = array(
          "status" => 203,
          "type" => FALSE,
          "msg" => "Query Duplicate",
          "data" => $sql
        );
      }else {
        $callback = array(
          "status" => 202,
          "type" => FALSE,
          "msg" => "Query Duplicate",
          "data" => $sql
        );
      }
    }
    return $callback;
  }

  public function favorite_job($uid,$announce_id)
  {
    $ckh = $this->chk_favorite($uid,$announce_id);
    if ($ckh['status'] == '202') {
      $sql="UPDATE favorite_job
            SET status = '0'
            WHERE announce_id = '$announce_id' AND uid = '$uid'";
      $qry = $this->db_job->query($sql);
      if ($qry) {
        $callback = array(
          "status" => 202,
          "type" => TRUE,
          "msg" => "OK",
        );
      }else {
        $callback = array(
          "status" => 201,
          "type" => FALSE,
          "msg" => "Query Error",
          "data" => $sql
        );
      }
    }
    elseif ($ckh['status'] == '203') {
      $sql="UPDATE favorite_job
            SET status = '1'
            WHERE announce_id = '$announce_id' AND uid = '$uid'";
      $qry = $this->db_job->query($sql);
      if ($qry) {
        $callback = array(
          "status" => 200,
          "type" => TRUE,
          "msg" => "OK",
        );
      }else {
        $callback = array(
          "status" => 201,
          "type" => FALSE,
          "msg" => "Query Error",
          "data" => $sql
        );
      }
    }else {
    $sql = "INSERT INTO favorite_job VALUES('$announce_id','$uid','$this->now','1')";
    $qry=$this->db_job->query($sql);
      if($qry){
        $this->countFavorite($announce_id);
        $this->Functions->insertLog('',$uid,$announce_id,"Favorite Job", "ติดตามงาน", "Applicant","");
        $callback = array(
          "status" => 200,
          "type" => TRUE,
          "msg" => "OK",
        );
      }else{
        $callback = array(
          "status" => 201,
          "type" => FALSE,
          "msg" => "Query Error",
          "data" => $sql
        );
      }
    }
    return $callback;

  }

  public function chk_favorite_me($uid,$announce_id)
  {
    $sql = "SELECT * FROM favorite_job WHERE announce_id = '$announce_id' AND uid = '$uid' AND status = '1'";
    $qry=$this->db_job->query($sql);
    if($qry->num_rows() > 0){
      return TRUE;
    }else{
      return FALSE;
    }
  }

  public function countView($announce_id,$uid,$infor)
  {
    // print_r($uid);exit;
    $sqlcount = "SELECT count_view FROM announce WHERE announce_id = '$announce_id'";
    $qrycount = $this->db_job->query($sqlcount);
    $count = $qrycount->result_array()[0]['count_view'];
    $count_view = intval($count)+1;
    $sql="UPDATE announce
          SET count_view = $count_view
          WHERE announce_id = '$announce_id'";
    $qry = $this->db_job->query($sql);
    if ($qry) {
      $this->Functions->insertLog('',$uid,$announce_id,"View", "ดูประกาศ", "Announce",$infor);
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }
  public function countApply($announce_id)
  {
    $sqlcount = "SELECT count_apply FROM announce WHERE announce_id = '$announce_id'";
    $qrycount = $this->db_job->query($sqlcount);
    $count = $qrycount->result_array()[0]['count_apply'];
    $count_apply = intval($count)+1;
    $sql="UPDATE announce
          SET count_apply = $count_apply
          WHERE announce_id = '$announce_id'";
    $qry = $this->db_job->query($sql);
  }
  public function countFavorite($announce_id)
  {
    $sqlcount = "SELECT count_favorite FROM announce WHERE announce_id = '$announce_id'";
    $qrycount = $this->db_job->query($sqlcount);
    $count = $qrycount->result_array()[0]['count_favorite'];
    $count_favorite = intval($count)+1;
    $sql="UPDATE announce
          SET count_favorite = $count_favorite
          WHERE announce_id = '$announce_id'";
    $qry = $this->db_job->query($sql);
  }

  public function DashboardCount($company_id,$uid)
  {
    $sql = "SELECT * FROM announce";
    if ($company_id != "") {
      $sql .= " WHERE company_id = '$company_id'";
    }
    $qry = $this->db_job->query($sql);
    // $package = $this->Company->getPackageByCompany($company_id);
    $announce_me = $this->getannounceByCreate($company_id,$uid);
    $dataset = array('announce' => $qry->num_rows(),
                     'announce_me' => $announce_me['count']
                    );
    if ($qry) {
     $callback =array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "data" => $dataset
      );
    }else {
      $callback = array(
        "status" => 400,
        "type" => FALSE,
        "msg" => "Insert Failed",
      );
    }
    return $callback;

  }

  public function DashboardTop10view($company_id,$uid)
  {
    $sql = "SELECT * FROM announce WHERE status = '1'";
    if ($company_id != "") {
      $sql .= "AND company_id = '$company_id'";
    }
    $sql .= "ORDER BY count_view  DESC LIMIT 10";
    $qry = $this->db_job->query($sql);
    if ($qry) {
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "data" => $qry->result_array()
      );
    }else {
      $callback = array(
        "status" => 400,
        "type" => FALSE,
        "msg" => "Insert Failed",
      );
    }
    return $callback;
  }

  public function DashboardTop10Apply($company_id,$uid)
  {
    $sql = "SELECT * FROM announce";
    if ($company_id != "") {
      $sql .= " WHERE company_id = '$company_id'";
    }

    $sql .= "ORDER BY count_apply DESC LIMIT 10";
    $qry = $this->db_job->query($sql);
    if ($qry) {
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "data" => $qry->result_array()
      );
    }else {
      $callback = array(
        "status" => 400,
        "type" => FALSE,
        "msg" => "Insert Failed",
      );
    }
    return $callback;

  }

  public function DashboardTop10Favorite($company_id,$uid)
  {
    $sql = "SELECT * FROM announce";
    if ($company_id != "") {
      $sql .= " WHERE company_id = '$company_id'";
    }
    $sql .= "ORDER BY count_favorite DESC LIMIT 10";
    $qry = $this->db_job->query($sql);
    if ($qry) {
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "data" => $qry->result_array()
      );
    }else {
      $callback = array(
        "status" => 400,
        "type" => FALSE,
        "msg" => "Insert Failed",
      );
    }

  }

  public function getAnnounceById($announce_id)
  {
    $sql = "SELECT announce.*
            FROM announce
            LEFT JOIN company ON announce.company_id = company.company_id
            LEFT JOIN company_detail  ON announce.company_id = company_detail.company_id
            WHERE status = '1' AND announce.announce_id = '$announce_id'";
    $qry = $this->db_job->query($sql);
    $dataset = $qry->result_array();
    for ($i=0; $i < $qry->num_rows(); $i++) {
      $dataset[$i]["percent"] = "";
      $dataset[$i]["company"] = $this->Company->getDeail($dataset[$i]['company_id'])['data'];
      // $dataset[$i]["job_description_2"] = $this->Master->getjob_description_2ById($dataset[$i]['job_description_1'],$dataset[$i]['job_description_2']);
      // $dataset[$i]["job_description_1"] = $this->Master->getjob_description_1ById($dataset[$i]['job_description_1']);
      $dataset[$i]["job_description_1"] = $this->Master->getPositionById($dataset[$i]['job_description_1']);
      $dataset[$i]["education"] = $this->Master->geteducationById($dataset[$i]['education']);
      $dataset[$i]["working_day"] = $this->Master->getworkingdayById($dataset[$i]['working_day']);
      $dataset[$i]["gender"] = $this->Master->getGenderById($dataset[$i]['gender']);
      $dataset[$i]["license"] = $this->Master->getlicenseById($dataset[$i]["license"]);
      $dataset[$i]["create_by"] = $this->Users->getUserById($dataset[$i]["create_by"])["data"];
      $dataset[$i]["type_job"] = $this->Master->getjob_typeById($dataset[$i]['type_job']);
      $dataset[$i]['image'] = $this->path."assets/documents/Announce/".$dataset[$i]['image'];
      $dataset[$i]["zone_detail"] = $this->Master->aumphurById($dataset[$i]['zone'],$dataset[$i]['zone_detail']);
      $dataset[$i]["zone"] = $this->Master->provinceById($dataset[$i]['zone']);

    }
    if ($qry) {
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "count" =>$qry->num_rows(),
        "data" => $dataset,
      );
    }else {
      $callback = array(
        "status" => 400,
        "type" => FALSE,
        "msg" => "Insert Failed",
      );
    }
    return $callback;
  }

  public function getRegisterByAnnounce($announce_id)
  {
    $sql = "SELECT * FROM apply_job WHERE announce_id = '$announce_id'";
    $qry = $this->db_job->query($sql);
    $dataset = $qry->result_array();
    for ($i=0; $i < $qry->num_rows() ; $i++) {
      $dataApplicant = $this->Applicant->getApplicantByUid($dataset[$i]["uid"]);
      $dataset[$i]["data"] = $dataApplicant["data"];
    }
    if ($qry) {
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "count" =>$qry->num_rows(),
        "data" => $dataset,
      );
    }else {
      $callback = array(
        "status" => 400,
        "type" => FALSE,
        "msg" => "Insert Failed",
        "count" =>$qry->num_rows(),
        "data" => ""
      );
    }
    return $callback;
  }

  public function getViewByAnnounce($announce_id)
  {
    $sql = "SELECT *  ,count(uid) as view FROM log_user WHERE passive = '$announce_id' AND action_id = '1' GROUP BY uid";
    // $sql = "SELECT *,count(uid) FROM log_system where passive = '$announce_id' AND log_type = 'view' GROUP BY uid";
    $qry = $this->db_job->query($sql);
    if ($qry->num_rows() > 0) {
      $dataset = $qry->result_array();
      for ($i=0; $i < $qry->num_rows() ; $i++) {
        if ($dataset[$i]["uid"] == '') {

        }else {
          $dataApplicant = $this->Applicant->getApplicantByUid($dataset[$i]["uid"]);
        }
        $dataset[$i]["data"] = $dataApplicant["data"];
      }
      if ($qry) {
        $callback = array(
          "status" => 200,
          "type" => TRUE,
          "msg" => "OK",
          "count" =>$qry->num_rows(),
          "data" => $dataset,
        );
      }else {
        $callback = array(
          "status" => 400,
          "type" => FALSE,
          "msg" => "Insert Failed",
        );
      }
    }else {
      $callback = array(
        "status" => 404,
        "type" => FALSE,
        "msg" => "Not Found",
        "count" =>$qry->num_rows(),
        "data" => ""
      );
    }
    return $callback;
  }

  public function getHiringCarousel()
  {
    $sql = "SELECT * FROM carousel WHERE status = '1' ORDER BY create_date";
    $qry = $this->db_job->query($sql);
    $rows = $qry->result_array();
    for ($i=0; $i < $qry->num_rows(); $i++) {
      $rows[$i]['image'] = $this->path."assets/images/hiring/".$rows[$i]['image'];
    }
    if ($qry) {
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "count" =>$qry->num_rows(),
        "data" => $rows,
      );
    }else {
      $callback = array(
        "status" => 404,
        "type" => FALSE,
        "msg" => "Error",
      );
    }
    return $callback;
  }
  public function CountAnnounce()
  {
    $sql = "SELECT announce.*
            FROM announce
            LEFT JOIN company ON announce.company_id = company.company_id
            LEFT JOIN company_detail  ON announce.company_id = company_detail.company_id
            WHERE announce.status = '1' AND company.company_status = '1' ORDER BY update_at DESC ";
    $qry = $this->db_job->query($sql);
    if ($qry) {
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "count" =>$qry->num_rows(),
      );
    }else {
      $callback = array(
        "status" => 404,
        "type" => FALSE,
        "count" => 0,
        "msg" => "Error",
      );
    }
    return $callback;
  }
  public function CountAnnounceFavorite($uid)
  {
    if ($uid == "") {
      $callback = array(
        "status" => 404,
        "type" => FALSE,
        "msg" => "OK",
        "count" => 0,
      );

    }else {
      $sql = "SELECT announce.*
              FROM announce
              LEFT JOIN company ON announce.company_id = company.company_id
              LEFT JOIN company_detail  ON announce.company_id = company_detail.company_id
              WHERE announce_id IN (SELECT announce_id FROM favorite_job where uid = '$uid' and status = '1')";
      $qry = $this->db_job->query($sql);
      if ($qry) {
        $callback = array(
          "status" => 200,
          "type" => TRUE,
          "msg" => "OK",
          "count" =>$qry->num_rows(),
        );
      }else {
        $callback = array(
          "status" => 404,
          "type" => FALSE,
          "count" => 0,
          "msg" => "Error",
        );
      }
    }

    return $callback;
  }
}
