<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Master extends CI_Model{

  function __construct()
  {
    parent::__construct();
    $this->db_job = $this->load->database('Job',TRUE);
    $this->db_recruitment= $this->load->database('Recruitment',TRUE);
    $this->now = $this->Functions->date_time_get();
  }

  public function province()
  {
      $sql = "SELECT * FROM ms_province";
      $qry  = $this->db_job->query($sql);
      if($qry){
        $callback = array(
         "status" => 200,
         "type" => TRUE,
         "msg" => "OK",
         "data" => $qry->result_array()
        );
      }else{
       $callback = array(
         "status" => 201,
         "type" => FALSE,
         "msg" => "Query Error",
         "data" => $sql
        );
      }
      return $callback;
  }
  public function aumphur($province)
  {
    $sql = "SELECT * FROM ms_aumphur WHERE pro_id = '$province'";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $callback = array(
       "status" => 200,
       "type" => TRUE,
       "msg" => "OK",
       "data" => $qry->result_array()
      );
    }else{
     $callback = array(
       "status" => 201,
       "type" => FALSE,
       "msg" => "Query Error",
       "data" => $sql
      );
    }
    return $callback;
  }
  public function tumbon($province,$aumphur)
  {
    $sql = "SELECT * FROM ms_tumbon WHERE pro_id = '$province' AND aumphur_id = '$aumphur'";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $callback = array(
       "status" => 200,
       "type" => TRUE,
       "msg" => "OK",
       "data" => $qry->result_array()
      );
    }else{
     $callback = array(
       "status" => 201,
       "type" => FALSE,
       "msg" => "Query Error",
       "data" => $sql
      );
    }
    return $callback;
  }
  public function provinceById($province)
  {
      $sql = "SELECT * FROM z_ms_province WHERE pro_id ='$province'";
      $qry  = $this->db_recruitment->query($sql);
      if($qry){
        $callback = array(
         "status" => 200,
         "type" => TRUE,
         "msg" => "OK",
         "data" => $qry->result_array()
        );
      }else{
       $callback = array(
         "status" => 201,
         "type" => FALSE,
         "msg" => "Query Error",
         "data" => $sql
        );
      }
      return $callback;
  }
  public function aumphurById($province,$aumphur)
  {
    $sql = "SELECT * FROM z_ms_district WHERE pro_id = '$province' AND district_id = '$aumphur'";
    $qry  = $this->db_recruitment->query($sql);
    if($qry){
      $callback = array(
       "status" => 200,
       "type" => TRUE,
       "msg" => "OK",
       "data" => $qry->result_array()
      );
    }else{
     $callback = array(
       "status" => 201,
       "type" => FALSE,
       "msg" => "Query Error",
       "data" => $sql
      );
    }
    return $callback;
  }
  public function tumbonById($province,$aumphur,$tumbon)
  {
    $sql = "SELECT * FROM ms_tumbon
    WHERE pro_id = '$province' AND aumphur_id = '$aumphur' AND id = '$tumbon'";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $callback = array(
       "status" => 200,
       "type" => TRUE,
       "msg" => "OK",
       "data" => $qry->result_array()
      );
    }else{
     $callback = array(
       "status" => 201,
       "type" => FALSE,
       "msg" => "Query Error",
       "data" => $sql
      );
    }
    return $callback;
  }
  public function getLvData()
  {
    $sql = "SELECT * FROM ms_level where id > 1";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "data" => $qry->result_array()
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }
  public function getLvById($lv_id)
  {
    $sql = "SELECT * FROM ms_level WHERE id = '$lv_id'";
    $qry = $this->db_job->query($sql);
    return $qry->result_array();
  }
  public function getjob_description_1()
  {
    $sql = "SELECT * FROM ms_job_description_1 WHERE is_active = 'Y' ORDER BY job_description_name ASC";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "data" => $qry->result_array()
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }
  public function getjob_description_2($job1)
  {
    $sql = "SELECT * FROM ms_job_description_2 WHERE is_active = 'Y' AND job_description_code_1 = '$job1'";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "data" => $qry->result_array()
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }
  public function getPosition()
  {
    $sql = "SELECT * FROM ir_ms_position";
    $qry  = $this->db_recruitment->query($sql);
    if($qry){
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "data" => $qry->result_array()
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }
  public function getZone()
  {
    $sql = "SELECT * FROM ms_zone ";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "data" => $qry->result_array()
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }
  public function getgender()
  {
    $sql = "SELECT * FROM ms_gender ";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "data" => $qry->result_array()
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }
  public function getjob_type()
  {
    $sql = "SELECT * FROM ms_job_type ";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "data" => $qry->result_array()
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }
  public function getlicense()
  {
    $sql = "SELECT * FROM ms_driving_license ";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "data" => $qry->result_array()
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }
  public function geteducation()
  {
    $sql = "SELECT * FROM ms_education ";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "data" => $qry->result_array()
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }
  public function getworking_day()
  {
    $sql = "SELECT * FROM ms_working_day ";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "data" => $qry->result_array()
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }
  public function getcompany_kind()
  {
    $sql = "SELECT * FROM ms_kind_company ";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "data" => $qry->result_array()
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }
  public function getcompany_kindById($id)
  {
    $sql = "SELECT * FROM ms_kind_company WHERE id = '$id'";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $callback = array(
        "status" => 200,
        "type" => TRUE,
        "msg" => "OK",
        "data" => $qry->result_array()
      );
    }else{
      $callback = array(
        "status" => 201,
        "type" => FALSE,
        "msg" => "Query Error",
        "data" => $sql
      );
    }
    return $callback;
  }
  public function getCompanyAll()
  {
    $sql = "SELECT company_name FROM company WHERE company_status = '1'";
    $qry  = $this->db_job->query($sql);
    if($qry){
      $callback = array(
       "status" => 200,
       "type" => TRUE,
       "msg" => "OK",
       "data" => $qry->result_array()
      );
    }else{
     $callback = array(
       "status" => 201,
       "type" => FALSE,
       "msg" => "Query Error",
       "data" => $sql
      );
    }
    return $callback;
  }
  public function getjob_description_1ById($id)
  {
    $sql = "SELECT * FROM ms_job_description_1 WHERE is_active = 'Y' AND job_description_code_1 = '$id' ORDER BY job_description_name ASC";
    $qry  = $this->db_job->query($sql);
    return $qry->result_array();
  }
  public function getjob_description_2ById($job1,$id)
  {
    $sql = "SELECT * FROM ms_job_description_2 WHERE is_active = 'Y' AND job_description_code_1 = '$job1' AND job_description_code_2 = '$id' ";
    $qry  = $this->db_job->query($sql);
    return $qry->result_array();
  }
  public function getPositionById($id)
  {
    $sql = "SELECT * FROM ir_ms_position WHERE position_id = '$id'";
    $qry  = $this->db_recruitment->query($sql);
    return $qry->result_array();
  }
  public function geteducationById($id)
  {
    $sql = "SELECT * FROM ms_education WHERE id = $id ";
    $qry  = $this->db_job->query($sql);
    return $qry->result_array();
  }
  public function getworkingdayById($id)
  {
    $sql = "SELECT * FROM ms_working_day WHERE id = '$id'";
    $qry  = $this->db_job->query($sql);
    return $qry->result_array();
  }
  public function getGenderById($id)
  {
    $sql = "SELECT * FROM ms_gender WHERE id = '$id'";
    $qry  = $this->db_job->query($sql);
    return $qry->result_array();
  }
  public function getlicenseById($id)
  {
    $sql = "SELECT * FROM ms_driving_license WHERE driving_license_type_code = '$id'";
    $qry  = $this->db_job->query($sql);
    return $qry->result_array();
  }
  public function getZoneById($id)
  {
    $sql = "SELECT * FROM ms_zone WHERE id = '$id'";
    $qry  = $this->db_job->query($sql);
    return $qry->result_array();
  }
  public function getjob_typeById($id)
  {
    $sql = "SELECT * FROM ms_job_type WHERE id = '$id'";
    $qry  = $this->db_job->query($sql);
    return $qry->result_array();
  }
  public function getms_nationalityById($id)
  {
    $sql = "SELECT * FROM ms_nationality WHERE id = '$id'";
    $qry  = $this->db_job->query($sql);
    return $qry->result_array();
  }
  public function getms_pfixById($id)
  {
    $sql = "SELECT * FROM ms_pfix WHERE id = '$id'";
    $qry  = $this->db_job->query($sql);
    return $qry->result_array();
  }
  public function getms_genderById($id)
  {
    $sql = "SELECT * FROM ms_gender WHERE id = '$id'";
    $qry  = $this->db_job->query($sql);
    return $qry->result_array();
  }
  public function getms_marital_statusById($id)
  {
    $sql = "SELECT * FROM ms_marital_status WHERE id = '$id'";
    $qry  = $this->db_job->query($sql);
    return $qry->result_array();
  }
  public function getms_religionById($id)
  {
    $sql = "SELECT * FROM ms_religion WHERE id = '$id'";
    $qry  = $this->db_job->query($sql);
    return $qry->result_array();
  }
  public function gethr_staff()
  {
    $sql = "SELECT * FROM hr_staff WHERE department_code = 'RM'";
    $qry  = $this->db_recruitment->query($sql);
    if($qry){
      $callback = array(
       "status" => 200,
       "type" => TRUE,
       "msg" => "OK",
       "data" => $qry->result_array()
      );
    }else{
     $callback = array(
       "status" => 201,
       "type" => FALSE,
       "msg" => "Query Error",
       "data" => $sql
      );
    }
    return $callback;
  }
  public function getms_information()
  {

    $sql = "SELECT * FROM ir_register_ms_information";
    $qry  = $this->db_recruitment->query($sql);
    if($qry){
      $callback = array(
       "status" => 200,
       "type" => TRUE,
       "msg" => "OK",
       "data" => $qry->result_array()
      );
    }else{
     $callback = array(
       "status" => 201,
       "type" => FALSE,
       "msg" => "Query Error",
       "data" => $sql
      );
    }
    return $callback;
  }
}
