<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require (APPPATH.'libraries/REST_Controller.php');
use Restserver\Libraries\REST_Controller;

class Company_controller extends REST_Controller {
// class Hello_controller extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Functions');
		$this->load->model('Announce');
		$this->load->model('Token');
		if($_SERVER["REQUEST_METHOD"] != "OPTIONS"){
	            $callback["options"] = false;
							if (!isset($_SERVER["HTTP_TOKEN"])) {
								$callback	= $callback = array(
														                        "status" => 404,
														                        "type" => FALSE,
														                        "msg" => "Not Found Token",
														                      	);
									$this->output->set_output(json_encode($callback));
							}else {
								$token = $_SERVER["HTTP_TOKEN"];
								$user = $this->Token->decode($token);
								$this->company_id = $user->company_id;
								$this->uid = $user->uid;
								$this->lv = $user->level;
							}
	  }
		else{
	        exit;
	  }
	}

	public function updateCreateCompany_post()
	{
		$callback = $this->Company->updateCreateCompany($company_id,$uid);
		$this->output->set_output(json_encode($callback));
	}

	public function CompanyDetail_post()
	{
		$company_name = $_POST['company_name'];
		$company_detail = $_POST["company_detail"];
		$company_kind =  $_POST["company_kind"];
		$tin = $_POST["tin"];
		$company_kind_detail =  $_POST["company_kind_detail"];
		$company_address =  $_POST["company_address"];
		$pro_id =  $_POST["pro_id"];
		$aumphur_id = $_POST["aumphur_id"];
		$tumbon_id = $_POST["tumbon_id"];
		$web_site = $_POST["web_site"];
		$company_tel = $_POST["company_tel"];
		$travel =  $_POST["travel"];
		$company_edit =$this->Company->editCompany($this->company_id,$company_name, $tin,$this->uid);
		if ($company_edit['status']==200) {
		$callback = $this->Company->createDetail($this->company_id,$company_detail,
		$company_kind, $company_kind_detail, $company_address,
		$pro_id, $aumphur_id,$tumbon_id, $web_site, $company_tel, $travel,$this->uid);
		$this->output->set_output(json_encode($callback));
		}else {
			$this->output->set_output(json_encode($company_edit));
		}
	}

	public function EditCompanyDetail_post()
	{
		$company_name = $_POST['company_name'];
		$company_detail = $_POST['company_detail'];
		$company_kind = $_POST['company_kind'];
		$company_kind_detail = $_POST['company_kind_detail'];
		$company_address = $_POST['company_address'];
		$tin = $_POST["tin"];
		$pro_id = $_POST['pro_id'];
		$aumphur_id = $_POST['aumphur_id'];
		$tumbon_id = $_POST['tumbon_id'];
		$web_site = $_POST['web_site'];
		$company_tel = $_POST['company_tel'];
		$travel = $_POST['travel'];
		$company_edit =$this->Company->editCompany($this->company_id,$company_name, $tin,$this->uid);
		if ($company_edit['status']==200) {
			$callback = $this->Company->editDetail($this->company_id,$this->uid,$company_detail, $company_kind, $company_kind_detail, $company_address, $pro_id, $aumphur_id,$tumbon_id, $web_site, $company_tel, $travel);
			$this->output->set_output(json_encode($callback));
		}else {
			$this->output->set_output(json_encode($company_edit));
		}
	}

	public function getCompanyDetail_get()
	{
		$callback = $this->Company->getDeail($this->company_id);
		$this->output->set_output(json_encode($callback));
	}
	public function getProfileID_post()
	{
		$uid = $_POST['uid'];
		$callback = $this->Applicant->getProfile($uid);
		$this->output->set_output(json_encode($callback));
	}
	public function GetActivityByUid_post()
	{
		$uid = $_POST['uid'];
		$callback = $this->Applicant->GetActivityByUid($uid);
		$this->output->set_output(json_encode($callback));
	}

	public function CountActivityByUid_post()
	{
		$uid = $_POST['uid'];
		$callback = $this->Applicant->CountActivityByUid($uid);
		$this->output->set_output(json_encode($callback));
	}

	public function Announce_files_post()
	{
		$file = $_FILES['file'];
		$id = $_POST['id'];
		$callback = $this->Announce->file_announce($id,$file);
		$this->output->set_output(json_encode($callback));
	}

	public function AnnounceAdd_post()
	{
		$image = "";
		$age_max = $_POST['age_max'];
		$age_min = $_POST['age_min'];
		$announce_title = $_POST['announce_title'];
		$benefits = $_POST['benefits'];
		$detail = $_POST['detail'];
		$education = $_POST['education'];
		$experience = $_POST['experience'];
		$gender = $_POST['gender'];
		$income_max = $_POST['income_max'];
		$income_min = $_POST['income_min'];
		$license = $_POST['license'];
		$property = $_POST['property'];
		$remark= $_POST['remark'];
		$type_job = $_POST['type_job'];
		$working_day = ($_POST['working_day'] == '' ? 99 : $_POST['working_day']);
		$working_start = $_POST['working_start'];
		$working_end = $_POST['working_end'];
		$working_start_date  = $_POST['working_start_date'];
		$working_end_date = $_POST['working_end_date'];
		$zone = ($_POST['zone'] == '' ? 0 : $_POST['zone']);
		// $zone_detail = "";
		$company_id = $this->company_id;
		$create_by = $this->uid;
		$job_description_1 = $_POST['job_description_1'];
		$job_description_2 = $_POST['job_description_2'];
		$zone_detail = $_POST['zone_detail'];
		$staff_id = $_POST['staff_id'];
		$callback = $this->Announce->add(
			 $image, $announce_title, $income_min,
		   $income_max, $type_job, $working_start_date, $working_end_date,$working_day, $working_start, $working_end,
		   $benefits, $detail, $property, $experience, $education, $age_min,
		   $age_max, $gender, $license, $zone, $zone_detail, $remark, $company_id,
		   $create_by,$job_description_1,$job_description_2,$staff_id
		 );
		$this->output->set_output(json_encode($callback));

	}
	public function AnnounceEdit_post()
	{
		$image = "";
		$announce_id = $_POST['announce_id'];
		$age_max = $_POST['age_max'];
		$age_min = $_POST['age_min'];
		$announce_title = $_POST['announce_title'];
		$benefits = $_POST['benefits'];
		$detail = $_POST['detail'];
		$education = $_POST['education'];
		$experience = $_POST['experience'];
		$gender = $_POST['gender'];
		$income_max = $_POST['income_max'];
		$income_min = $_POST['income_min'];
		$license = $_POST['license'];
		$property = $_POST['property'];
		$remark= $_POST['remark'];
		$type_job = $_POST['type_job'];
		$working_day = ($_POST['working_day'] == '' ? 99 : $_POST['working_day']);
		$working_start = $_POST['working_start'];
		$working_end = $_POST['working_end'];
		$working_start_date  = $_POST['working_start_date'];
		$working_end_date = $_POST['working_end_date'];
		$zone = ($_POST['zone'] == '' ? 0 : $_POST['zone']);
		$zone_detail = "";
		$company_id = $this->company_id;
		$update_by = $this->uid;
		$job_description_1 = $_POST['job_description_1'];
		$job_description_2 = $_POST['job_description_2'];
		$zone_detail = $_POST['zone_detail'];
		$staff_id = $_POST['staff_id'];
		$callback = $this->Announce->edit($announce_id,$image, $announce_title, $income_min,
	   $income_max, $type_job, $working_start_date, $working_end_date,
	   $working_day, $working_start, $working_end,
	   $benefits, $detail, $property, $experience, $education, $age_min,
	   $age_max, $gender, $license, $zone, $zone_detail, $remark,
	   $job_description_1,$job_description_2,$update_by,$company_id,$staff_id);
		$this->output->set_output(json_encode($callback));

	}

	public function company_files_post()
	{
		$file = $_FILES['file'];
		$id = $_POST['id'];
		$callback = $this->Company->file_company($id,$file);
		$this->output->set_output(json_encode($callback));
	}

	public function company_detail_files_post()
	{
		$file = $_FILES['file'];
		$id = $_POST['id'];
		$callback = $this->Company->file_company_detail($id,$file);
		$this->output->set_output(json_encode($callback));
	}

	public function DashboardCount_get()
	{
		$callback = $this->Announce->DashboardCount($this->company_id,$this->uid);
		$this->output->set_output(json_encode($callback));
	}

	public function DashboardTop10view_get()
	{
		$callback = $this->Announce->DashboardTop10view($this->company_id,$this->uid);
		$this->output->set_output(json_encode($callback));
	}
	public function DashboardTop10Apply_get()
	{
		$callback = $this->Announce->DashboardTop10Apply($this->company_id,$this->uid);
		$this->output->set_output(json_encode($callback));
	}
	public function getannounceByCreate_get()
	{
		$callback = $this->Announce->getannounceByCreate($this->company_id,$this->uid);
		$this->output->set_output(json_encode($callback));
	}

	public function updateStatusTrash_post()
	{
		$announce_id = $_POST['announce_id'];
		$status = '0';
		$callback = $this->Announce->updateStatus($announce_id,$status,$this->company_id,$this->uid);
		$this->output->set_output(json_encode($callback));
	}

	public function getApplicantByAnnounce_get()
  {
    $announce_id = $_GET['announce_id'];
		$callback = $this->Announce->getRegisterByAnnounce($announce_id);
		$this->output->set_output(json_encode($callback));
  }

	public function getViewByAnnounce_get()
  {
    $announce_id = $_GET['announce_id'];
		$callback = $this->Announce->getViewByAnnounce($announce_id);
		$this->output->set_output(json_encode($callback));
  }

}
