<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require (APPPATH.'libraries/REST_Controller.php');
use Restserver\Libraries\REST_Controller;

class Authen_controller extends REST_Controller {
// class Hello_controller extends CI_Controller {
	public function __construct(){
		parent::__construct();
    $this->load->model('Company');
    $this->load->model('Users');
    $this->load->model('Login');
    $this->load->model('Announce');
    $this->load->model('Applicant');
    $this->load->model('Functions');
	}

	public function chkEmailDuplicate_get()
	{
		$username = $_GET['email'];
		$callback = $this->Users->chkEmailDuplicate($username);
		$this->output->set_output(json_encode($callback));
	}

  public function UsersCreate_post()
  {
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $email = $_POST['email'];
    $tel = $_POST['tel'];
    $level = '1';
    $invite_by = "U000000000000";
    $company_id = "";
    $callback = $this->Users->create($company_id, $fname, $lname, $email, $tel, $level, $invite_by);
    $this->output->set_output(json_encode($callback));
  }

  public function CompanyCreate_post()
  {
    $company_name = $_POST['company_name'];
    $tin = $_POST['tin'];
    $create_by = $_POST['uid'];
    $callback = $this->Company->create($company_name, $tin, $create_by);
		if ($callback['status']==200) {
			$callback = $this->Users->updateCreateCompany($callback['id'],$create_by);
			$this->output->set_output(json_encode($callback));
		}else {
			$this->output->set_output(json_encode($callback));
		}
  }

  public function updateCreateCompany_post()
  {
    $company_id = $_POST['comapany_id'];
    $uid = $_POST['uid'];
    $callback = $this->Users->updateCreateCompany($company_id,$uid);
    $this->output->set_output(json_encode($callback));
  }

  public function comfirm_post()
  {
    $uid = base64_decode(base64_decode(base64_decode($_POST['uid'])));
		$code = $_POST['code'];
		$password = $_POST['password'];
		$chkPassCode = $this->Users->chkPassCode($uid,$code);
		if ($chkPassCode['status'] == 200) {
			$callback = $this->Users->updatePassword($uid,$password,$uid);
			$this->Users->updateStatus($uid,'1',$uid);
			$this->output->set_output(json_encode($callback));
		}else {
			$this->output->set_output(json_encode($callback));
		}
  }

	public function chkStatus_post()
	{
		$uid = base64_decode(base64_decode(base64_decode($_POST['uid'])));
		$callback = $this->Users->chkStatus($uid);
		$this->output->set_output(json_encode($callback));
	}

  public function Login_post()
  {
    $email = $_POST['email'];
    $password = $_POST['password'];
    $callback = $this->Login->Login($email,$password);
    $this->output->set_output(json_encode($callback));
  }

  public function Logout_post()
	{
		$callback = array();
		if($_SERVER["REQUEST_METHOD"] != "OPTIONS"){
			$token = $_SERVER["HTTP_TOKEN"];
			$user = $this->Token->decode($token);
      $uid = $user->uid;
			$company_id = $user->company_id;
			$callback = $this->Login->Logout($company_id,$uid);
		}
		else{
			$callback = array(
												"status" => 404,
		                    "type" => FALSE,
		                    "msg" => "Not Found",
							 				 );
		}
		$this->output->set_output(json_encode($callback));
	}

  public function resetPassword_post()
  {
    $uid = base64_decode(base64_decode(base64_decode($_POST['uid'])));
    $password = $_POST['password'];
    $callback = $this->Users->updatePassword($uid,$password);
    $this->output->set_output(json_encode($callback));
  }

	public function getannounceconnect_get()
	{
		$uid;
		$token = $_SERVER["HTTP_TOKEN"];
		if ($token == "null" || $token == "") {
			$uid = "";
		}else {
			$user = $this->Token->decode($token);
			$uid = $user->uid;
		}
		$search_job_description_1 = $_GET["job_description_1"];
		$search_job_description_2= $_GET["job_description_2"];
		$search_type_job = $_GET["type_job"];
		$search_zone = $_GET["zone"];
		$search_zone_detail = '';
		$search_education = $_GET["education"];
		$search_companyname = $_GET["company_name"];
		$offset = $_GET["offset"];
		$callback = $this->Announce->getannounce_search($uid,$search_job_description_1,$search_job_description_2,$search_type_job,$search_zone,$search_zone_detail,$search_education,$search_companyname,$offset);
    $this->output->set_output(json_encode($callback));
	}
	public function getannounceAll_get()
	{
		$uid;
		$token = $_SERVER["HTTP_TOKEN"];
		if ($token == "null" || $token == "") {
			$uid = "";
		}else {
			$user = $this->Token->decode($token);
			$uid = $user->uid;
		}
		$callback = $this->Announce->getannounceAll($uid);
    $this->output->set_output(json_encode($callback));
	}
	public function getAnnounce_content_get()
	{
		$uid;
		$token = $_SERVER["HTTP_TOKEN"];
		if ($token == "null" || $token == "") {
			$uid = "";
		}else {
			$user = $this->Token->decode($token);
			$uid = $user->uid;
		}
		$callback = $this->Announce->getAnnounce_content($uid);
		$this->output->set_output(json_encode($callback));
	}
	public function getannounce_topview_get()
	{
		$uid;
		$token = $_SERVER["HTTP_TOKEN"];
		if ($token == "null" || $token == "") {
			$uid = "";
		}else {
			$user = $this->Token->decode($token);
			$uid = $user->uid;
		}
		$callback = $this->Announce->getannounce_topview($uid);
    $this->output->set_output(json_encode($callback));
	}
	public function getannounce_topfavorite_get()
	{
		$uid;
		$token = $_SERVER["HTTP_TOKEN"];
		if ($token == "null" || $token == "") {
			$uid = "";
		}else {
			$user = $this->Token->decode($token);
			$uid = $user->uid;
		}
		$callback = $this->Announce->getannounce_topfavorite($uid);
    $this->output->set_output(json_encode($callback));
	}
	public function getannounce_topincome_get()
	{
		$uid;
		$token = $_SERVER["HTTP_TOKEN"];
		if ($token == "null" || $token == "") {
			$uid = "";
		}else {
			$user = $this->Token->decode($token);
			$uid = $user->uid;
		}
		$callback = $this->Announce->getannounce_topincome($uid);
    $this->output->set_output(json_encode($callback));
	}
	public function getannounce_month_get()
	{
		$uid;
		$token = $_SERVER["HTTP_TOKEN"];
		if ($token == "null" || $token == "") {
			$uid = "";
		}else {
			$user = $this->Token->decode($token);
			$uid = $user->uid;
		}
		$callback = $this->Announce->getannounce_month($uid);
    $this->output->set_output(json_encode($callback));
	}
	public function getannounce_myfavorite_get()
	{

		$token = $_SERVER["HTTP_TOKEN"];
		if ($token == "null" || $token == "") {
			$uid = "";
		}else {
			$user = $this->Token->decode($token);
			$uid = $user->uid;
		}
		$callback = $this->Announce->getannounce_myfavorite($uid);
    $this->output->set_output(json_encode($callback));
	}
	public function getannounce_search_get()
	{
		$uid;
		$token = $_SERVER["HTTP_TOKEN"];
		if ($token == "null" || $token == "") {
			$uid = "";
		}else {
			$user = $this->Token->decode($token);
			$uid = $user->uid;
		}
		$search_job_description_1 = $_GET["job_description_1"];
		$search_job_description_2= $_GET["job_description_2"];
		$search_type_job = $_GET["type_job"];
		$search_zone = $_GET["zone"];
		$search_zone_detail = $_GET["zone_detail"];
		$search_education = $_GET["education"];
		$search_companyname = $_GET["company_name"];
		$offset = $_GET["offset"];
		$callback = $this->Announce->getannounce_search($uid,$search_job_description_1,$search_job_description_2,$search_type_job,$search_zone,$search_zone_detail,$search_education,$search_companyname,$offset);
    $this->output->set_output(json_encode($callback));
	}
	public function Chk_applicant_post()
	{
		$uid = $_POST['uid'];
		$callback = $this->Applicant->Chk_applicant($uid);
		if ($callback['status']== '202' ) {
			$chk_resume = $this->Applicant->Chk_resume($callback["data"][0]["citizen_id"]);
			if ($chk_resume['status'] == '200') {
				if ($callback["data"][0]["status"] == 9 ) {
					$status = ($chk_resume['status'] == '200' ? 1 : 9);
					$this->Applicant->updateStatus($status,$uid);
				}
			}
		}
    $this->output->set_output(json_encode($callback));
	}
	public function register_post()
	{
		$uid  = $_POST['uid'];
		$citizen_id = $_POST['citizen'];
		$fname = $_POST['fname'];
		$lname = $_POST['lname'];
		$tel = $_POST['tel'];
		$email = $_POST['email'];
		$img = $_POST['img'];
		$callback = $this->Applicant->Chk_applicant($uid);
		if ($callback['status'] == '202' ) {
			$update_citizen = $this->Applicant->update_applicant($uid,$citizen_id);
			$chk_resume = $this->Applicant->Chk_resume($citizen_id);
			$status = ($chk_resume['status'] == '200' ? 1 : 9);
			$update_status = $this->Applicant->updateStatus($status,$uid);
			$callback = $this->Login->loginApplicant($uid,'');
			$this->output->set_output(json_encode($callback));
		}else {
			$chk_resume = $this->Applicant->Chk_resume($citizen_id);
			$status = ($chk_resume['status'] == '200' ? 1 : 9);
			$register = $this->Applicant->applicant_register($uid,$fname,$lname,$tel,$email,$citizen_id,$status,$img);
			if ($register['status'] == '200') {
				$callback = $this->Login->loginApplicant($uid,'');
				$this->output->set_output(json_encode($callback));
			}else {
				$this->output->set_output(json_encode($register));
			}
		}
	}
	public function loginApplicant_post()
	{
		$uid = $_POST['uid'];
		$img = $_POST['img'];
		$citizen = $_POST['citizen'];
		$update_status = $this->Applicant->updateimgLine($uid,$img);
    $callback = $this->Login->loginApplicant($uid,$citizen);
    $this->output->set_output(json_encode($callback));
	}
	public function logoutApplicant_post()
	{
		$callback = array();
			if (!isset($_SERVER["HTTP_TOKEN"])) {
				$callback = array(
													"status" => 404,
													"type" => FALSE,
													"msg" => "Not Found",
												 );
			}else{
							$token = $_SERVER["HTTP_TOKEN"];
							$user = $this->Token->decode($token);
							$uid = $user->uid;
							$company_id = $user->company_id;
							$callback = $this->Login->logoutApplicant($uid);
			}
		$this->output->set_output(json_encode($callback));
	}
	public function countView_post()
	{
		$uid;
		$token = $_SERVER["HTTP_TOKEN"];
		if ($token == "null" || $token == "") {
			$uid = "";
		}else {
			$user = $this->Token->decode($token);
			$uid = $user->uid;
		}
		$announce_id  = $_POST['announce_id'];
		$infor = $_POST['infor'];
		$callback = $this->Announce->countView($announce_id,$uid,$infor);
		$this->output->set_output(json_encode($callback));
	}
	public function getAnnounceById_get()
	{
		$announce_id  = $_GET['announce_id'];
		$callback = $this->Announce->getAnnounceById($announce_id);
		$this->output->set_output(json_encode($callback));
	}
	public function getHiringCarousel_get()
	{
		$callback = $this->Announce->getHiringCarousel();
		$this->output->set_output(json_encode($callback));
	}
	public function logUser_post()
	{
		$uid;
		$token = $_POST["token"];
		if ($token == "") {
			$this->output->set_output('none uid');
		}else {
			$user = $this->Token->decode($token);
			$uid = $user->uid;
			$action_id = $_POST['action_id'];
			$passive = $_POST['passive'];
			$page = $_POST['page'];
			$job_type = $_POST['job_type'];
			$job_description_1 = $_POST['job_description_1'];
			$job_description_2 = $_POST['job_description_2'];
			$zone = $_POST['zone'];
			$education = $_POST['education'];
			$company = $_POST['company'];
			$infor = $_POST['infor'];
			$callback = $this->Functions->insertLogUser($uid,$action_id,$passive,$page,$job_type,$job_description_1,$job_description_2,$zone,$education,$company,$infor);
			$this->output->set_output(json_encode($callback));
		}
	}
	public function CountAnnounce_post()
	{
		$callback = $this->Announce->CountAnnounce();
		$this->output->set_output(json_encode($callback));
	}
	public function CountAnnounceFavorite_post()
	{
		$uid;
		$token = $_POST["token"];
		if ($token == "") {
			$uid = "";
		}else {
			$user = $this->Token->decode($token);
			$uid = $user->uid;
		}
		$callback = $this->Announce->CountAnnounceFavorite($uid);
		$this->output->set_output(json_encode($callback));
	}



}
