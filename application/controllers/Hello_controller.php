<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require (APPPATH.'libraries/REST_Controller.php');
use Restserver\Libraries\REST_Controller;

class Hello_controller extends REST_Controller {
// class Hello_controller extends CI_Controller {
	public function __construct(){
		parent::__construct();
	}

	public function hello_post()
	{
		$callback = $this->Hello->hello();
		$this->output->set_output(json_encode($callback));
		
	}

}