<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require (APPPATH.'libraries/REST_Controller.php');
use Restserver\Libraries\REST_Controller;

class Applicant_controller extends REST_Controller {
// class Hello_controller extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Functions');
		$this->load->model('Announce');
		$this->load->model('Applicant');
		$this->load->model('Token');
		if($_SERVER["REQUEST_METHOD"] != "OPTIONS"){
	            $callback["options"] = false;
							if (!isset($_SERVER["HTTP_TOKEN"])) {
								$callback	= $callback = array(
														                   "status" => 404,
														                   "type" => FALSE,
														                   "msg" => "Not Found Token",
														                  );
									$this->output->set_output(json_encode($callback));
							}else {
								$token = $_SERVER["HTTP_TOKEN"];
								$user = $this->Token->decode($token);
								$this->uid = $user->uid;
								$this->citizen = $user->citizen;
								$this->job_description_1 = $user->job_description_1;
								$this->job_description_2 = $user->job_description_2;
								$this->birth_date = $user->birth_date;
								$this->zone_id = $user->zone_id;
								$this->degree_code = $user->degree_code;
							}
	  }
		else{
	        exit;
	  }
	}

	public function apply_job_post()
	{
		$announce_id  = $_POST['announce_id'];
		$userrow = $this->Applicant->getProfile($this->uid)['data'];
		$citizen = $userrow['id_card'];
		$uid = $this->uid ;
		$callback = $this->Announce->apply_job($citizen,$uid,$announce_id);
		$this->output->set_output(json_encode($callback));
	}

	public function favorite_job_post()
	{
		$announce_id  = $_POST['announce_id'];
		$callback = $this->Announce->favorite_job($this->uid,$announce_id);
		$this->output->set_output(json_encode($callback));
	}

	public function getProfile_get()
	{
		$callback = $this->Applicant->getProfile($this->uid);
		$this->output->set_output(json_encode($callback));
	}

	public function getAnnounceMe_get()
	{
		$search_job_description_1 = $_GET["job_description_1"];
		$search_job_description_2= $_GET["job_description_2"];
		$search_type_job = $_GET["type_job"];
		$search_zone = $_GET["zone"];
		$search_education = $_GET["education"];
		$search_companyname = $_GET["company_name"];
		$offset = $_GET["offset"];
		$callback = $this->Applicant->getAnnounceWithPercentMatch($this->citizen,$this->job_description_1,$this->job_description_2,
		$this->birth_date,$this->zone_id,$this->degree_code,$search_job_description_1,$search_job_description_2,
		$search_type_job,$search_zone,$search_education,$search_companyname,$offset);
		$this->output->set_output(json_encode($callback));
	}

	public function chk_citizen_post()
	{
		$citizen = $this->citizen;
		$uid = $this->uid ;
		$callback = $this->Applicant->chk_citizen($uid,$citizen);
		$this->output->set_output(json_encode($callback));
	}

	public function update_applicant_post()
	{
		$citizen = $_POST['citizen'];
		$uid = $this->uid ;
		$province = $_POST['province'] ;
		$aumphur = $_POST['aumphur'];
		$birth_date = $_POST['birth_date'];
		$tel=$_POST['tel'];
		$callback = $this->Applicant->update_applicant($uid,$citizen,$province,$aumphur,$birth_date,$tel);
		$this->output->set_output(json_encode($callback));
	}
	public function edit_applicant_post()
	{
		$citizen = $_POST['citizen'];
		$uid = $this->uid ;
		$province = $_POST['province'] ;
		$aumphur = $_POST['aumphur'];
		$birth_date = $_POST['birth_date'];
		$tel = $_POST['tel'];
		$email = $_POST['email'];
		$callback = $this->Applicant->edit_applicant($uid,$citizen,$province,$aumphur,$birth_date,$tel,$email);
		$this->output->set_output(json_encode($callback));
	}

	public function chk_iRecruit_get()
	{
		$userrow = $this->Applicant->getProfile($this->uid)['data'];
		$citizen = $userrow['id_card'];
		$callback = $this->Announce->chk_iRecruit($citizen);
		$this->output->set_output(json_encode($callback));
	}

	public function register_iRecruit_post(){

		$staff_id = $_POST['staff_id'];
		$infor_id = $_POST['infor_id'];
		$position = $_POST['position'];
		$userrow = $this->Applicant->getProfile($this->uid)['data'];
		$province = $userrow['province'];
		$aumphur_id = $userrow['aumphur'];
		$citizen = $userrow['id_card'];
		$birth_date = $userrow['birth_date'];
		$fname = $userrow['fname'];
		$lname = $userrow['lname'];
		$mobile = $userrow['tel'];
		$pfix = "";
		$sex = "";
		$degree = "";
		$experience = "";

		$callback = $this->Announce->register_iRecruit($citizen,$pfix,$sex,$fname,$lname,$birth_date,$mobile,$degree,
	  $experience, $staff_id, $infor_id,$position,$province,$aumphur_id);
		$this->output->set_output(json_encode($callback));
	}

	public function chk_announce_iRecruit_post()
	{
		$announce_id  = $_POST['announce_id'];
		$reg_id = $_POST['reg_id'];
		$citizen = $this->citizen;
		$callback = $this->Announce->chk_announce_iRecruit($reg_id,$announce_id);
		$this->output->set_output(json_encode($callback));
	}

	public function chk_position_iRecruit_post()
	{
		$announce_id  = $_POST['announce_id'];
		$position = $_POST['position'];
		$reg_id = $_POST['reg_id'];
		$citizen = $this->citizen;
		$callback = $this->Announce->chk_position_iRecruit($reg_id,$position);
		$this->output->set_output(json_encode($callback));
	}



}
