<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require (APPPATH.'libraries/REST_Controller.php');
use Restserver\Libraries\REST_Controller;

class Users_controller extends REST_Controller {
// class Hello_controller extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Token');
		if($_SERVER["REQUEST_METHOD"] != "OPTIONS"){
	            $callback["options"] = false;
							if (!isset($_SERVER["HTTP_TOKEN"])) {
								$callback	= $callback = array(
														                   "status" => 404,
														                   "type" => FALSE,
														                   "msg" => "Not Found Token",
														                  );
									$this->output->set_output(json_encode($callback));
							}else {
								$token = $_SERVER["HTTP_TOKEN"];
								$user = $this->Token->decode($token);
								$this->company_id = $user->company_id;
								$this->uid = $user->uid;
								$this->lv = $user->level;
							}
	  }
		else{
	        exit;
	  }
	}

	public function InviteUsers_post()
	{
		$fname = $_POST['fname'];
		$lname = $_POST['lname'];
		$email = $_POST['email'];
		$tel = $_POST['tel'];
		$level = ($_POST['level'] == '' ? '3' : $_POST['level']);
		$invite_by = $this->uid;
		$callback = $this->Users->create($this->company_id, $fname, $lname, $email, $tel, $level, $invite_by);
		$this->output->set_output(json_encode($callback));
	}

	public function editUser_post()
	{
		$fname = $_POST['fname'];
		$lname = $_POST['lname'];
		$tel = $_POST['tel'];
		$uid = ($_POST['uid'] == '' ? $this->uid : $_POST['uid']);
		$update_by = $this->uid;
		$company_id = $this->company_id;
		$callback = $this->Users->edit($fname, $lname, $tel, $uid, $update_by,$company_id);
		$this->output->set_output(json_encode($callback));
	}

	public function profile_files_post()
	{
		$file = $_FILES['file'];
		$id = $_POST['id'];
		$callback = $this->Users->file_profile($id,$file);
		$this->output->set_output(json_encode($callback));
	}

	public function updateLevel_post()
	{
		$uid = $this->uid;
		$level = $_POST['level'];
		$callback = $this->Users->updateLevel($uid,$level);
		$this->output->set_output(json_encode($callback));
	}

	public function updateStatus_post()
	{
		$uid = $_POST['uid'];
		$status = $_POST['status'];
		$update_by = $this->uid;
		$callback = $this->Users->updateStatus($uid,$status,$update_by);
		$this->output->set_output(json_encode($callback));
	}

	public function getProfile_get()
	{
		$callback = $this->Users->getProfile($this->uid);
		$this->output->set_output(json_encode($callback));
	}

	public function getProfileID_get()
	{
		$uid = $_GET['uid'];
		$callback = $this->Users->getProfile($uid);
		$this->output->set_output(json_encode($callback));
	}

	public function UserByCompany_post()
	{
		$callback = $this->Users->getUserByCompany($this->company_id);
		$this->output->set_output(json_encode($callback));
	}

	public function DashboardCount_get()
	{
		$callback = $this->Users->DashboardCount($this->company_id);
		$this->output->set_output(json_encode($callback));
	}

	public function resendConfirm_post()
	{
		$uid = $_POST['uid'];
		$callback = $this->Email->SendMailConfirm($uid);;
		$this->output->set_output(json_encode($callback));
	}

}
