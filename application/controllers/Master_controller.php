<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require (APPPATH.'libraries/REST_Controller.php');
use Restserver\Libraries\REST_Controller;

class Master_controller extends REST_Controller {
// class Hello_controller extends CI_Controller {
	public function __construct(){
		parent::__construct();
	}

	public function ms_province_get()
	{
		$callback = $this->Master->province();
		$this->output->set_output(json_encode($callback));
	}

	public function ms_aumphur_get()
	{
		$province = $_GET['pro_id'];
		$callback = $this->Master->aumphur($province);
		$this->output->set_output(json_encode($callback));
	}

	public function ms_tumbon_get()
	{
		$province = $_GET['pro_id'];
		$aumphur = $_GET['aumphur_id'];
		$callback = $this->Master->tumbon($province,$aumphur);
		$this->output->set_output(json_encode($callback));
	}

	public function ms_level_get()
	{
		$callback = $this->Master->getLvData();
		$this->output->set_output(json_encode($callback));
	}

	public function ms_job_description_1_get()
	{
		$callback = $this->Master->getjob_description_1();
		$this->output->set_output(json_encode($callback));
	}

	public function ms_job_description_2_get()
	{
		$job1 = $_GET['job1'];
		$callback = $this->Master->getjob_description_2($job1);
		$this->output->set_output(json_encode($callback));
	}

	public function ms_zone_get()
	{
		$callback = $this->Master->getZone();
		$this->output->set_output(json_encode($callback));
	}

	public function ms_job_type_get()
	{
		$callback = $this->Master->getjob_type();
		$this->output->set_output(json_encode($callback));
	}

	public function ms_gender_get()
	{
		$callback = $this->Master->getgender();
		$this->output->set_output(json_encode($callback));
	}

	public function ms_license_get()
	{
		$callback = $this->Master->getlicense();
		$this->output->set_output(json_encode($callback));
	}

	public function ms_education_get()
	{
		$callback = $this->Master->geteducation();
		$this->output->set_output(json_encode($callback));
	}

	public function ms_working_day_get()
	{
		$callback = $this->Master->getworking_day();
		$this->output->set_output(json_encode($callback));
	}

	public function ms_company_kind_get()
	{
		$callback = $this->Master->getcompany_kind();
		$this->output->set_output(json_encode($callback));
	}
	public function ms_company_name_get()
	{
		$callback = $this->Master->getcompanyAll();
		$this->output->set_output(json_encode($callback));
	}
	public function ms_position_get()
	{
		$callback = $this->Master->getPosition();
		$this->output->set_output(json_encode($callback));
	}
	public function ms_staff_get()
	{
		$callback = $this->Master->gethr_staff();
		$this->output->set_output(json_encode($callback));
	}
	public function ms_information_get()
	{
		$callback = $this->Master->getms_information();
		$this->output->set_output(json_encode($callback));
	}






}
