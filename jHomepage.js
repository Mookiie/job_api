$(document).ready(function() {
  closeloader();
  var authtoken = localStorage.getItem('token');
  if(authtoken == null){
    getAnnounceAll(0);
  }else {
    getAnnounceMatch(0);
  }
});

function getAnnounceAll(data) {
  var api_url = getApi('getannounce');
  if (data) {
    var formData = {
                      "type_job":$('#type_job').val(),
                      "job_description_1":$('#job_description_1').val(),
                      "job_description_2":$('#job_description_2').val(),
                      "education":$('#education').val(),
                      "zone":$('#zone').val(),
                      "company_name":$('#company_name').val(),
                      "offset":'0'
                    };
  }else {
    var formData = {
                      "type_job":'',
                      "job_description_1":'',
                      "job_description_2":'',
                      "education":'',
                      "zone":'',
                      "company_name":'',
                      "offset":'0'
                    };
  }

  $.ajax({
    url: api_url,
    type: 'GET',
    data:formData,
    beforeSend:function(){
      preloader();
    }
  })
  .done(function(data) {
      closeloader()
      var obj = JSON.parse(data);
      console.log(obj);
      $(".closemodal").trigger('click');
      if (obj.count == 0) {
        getAnnounceAll(0)
      }else {
        for (var i = 0; i < obj.data.length; i++) {
            var row = genCardAnnounce(obj.data[i]);
            $(".content-announce").append(row)
            $('.tooltipped').tooltip();
        }
      }
  })
}

function getAnnounceMatch(data) {
  console.log("getAnnounceMatch");
  var api_url = getApi('applicant/getAnnounceMe');
  var token = localStorage.getItem('token');
  // if (data) {
  //   var formData = {
  //                     "type_job":$('#type_job').val(),
  //                     "job_description_1":$('#job_description_1').val(),
  //                     "job_description_2":$('#job_description_2').val(),
  //                     "education":$('#education').val(),
  //                     "zone":$('#zone').val(),
  //                     "company_name":$('#company_name').val(),
  //                     "offset":'999'
  //                   };
  // }else {
    var formData = {
                      "type_job":'',
                      "job_description_1":'',
                      "job_description_2":'',
                      "education":'',
                      "zone":'',
                      "company_name":'',
                      "offset":'999'
                    };
  // }
  $.ajax({
    url: api_url,
    type: 'GET',
    headers:{"token":token},
    data:formData,
    beforeSend:function(){
      preloader();
    }
  })
  .done(function(data) {
      closeloader()
      var obj = JSON.parse(data);
      console.log(obj);
      $(".closemodal").trigger('click');
      if (obj.count == 0) {
        getAnnounceAll(0)
        // getAnnounceMatch(0)
      }else {
        for (var i = 0; i < obj.data.length; i++) {
            var row = genCardAnnounce(obj.data[i]);
            $(".content-announce").append(row)
            $('.tooltipped').tooltip();
        }
      }
  })
}

function filter_Dialog() {
  console.log("filter_Dialog");
  var connectURL = getUrl("filterDialog");
  var formData = {"ajax":true};
      formData["data"] = "" ;
  $.ajax({
          url: connectURL,
          type: 'POST',
          data: formData
        }).done(function(data) {
          closeloader()
          var obj_modal = JSON.parse(data);
          $(".modal-area").append(obj_modal["modal"]);
          $("#filterDialog").Modal({"backdrop":true,"static":true});
          ms_job_type_get();
          ms_job_description_1_get();
          ms_zone_get();
          ms_education_get();
          $("#job_description_1").change(function() {
            ms_job_description_2_get($("#job_description_1").val());
            $("#job_description_2").removeAttr("disabled","");
          })
        })

}

function filter() {
  console.log("filter");
  var authtoken = localStorage.getItem('token');
  // $(".closemodal").trigger('click');
  if(authtoken == null){
    $(".content-announce").html('')
    getAnnounceAll(1);
  }else {
    $(".content-announce").html('')
    getAnnounceMatch(1);
  }

}

function count_view(announce_id) {
  var api_url = getApi('countView');
  $.ajax({
    url: api_url,
    type: 'POST',
    data:{"announce_id":announce_id},
  })
  .done(function(data) {
    // $(".announce_detail").css({"display":""})
    // $(".announce").css({"display":"none"})
    view_detail(announce_id)
  })
}

function view_detail(announce_id) {
  var authtoken = localStorage.getItem('token');
  var url = getApi('getAnnounceById');
  $.ajax({
    url: url,
    type: 'GET',
    headers:{"token":authtoken},
    data:{"announce_id":announce_id},
  })
  .done(function(data) {
    var obj = JSON.parse(data);
    var data_detail = obj.data[0];
    console.log(data_detail);
    var connectURL = getUrl("DetailDialog");
    var formData = {"ajax":true};
        formData["data"] = data_detail ;
    $.ajax({
            url: connectURL,
            type: 'POST',
            data: formData
          }).done(function(data) {
            closeloader()
            var obj_modal = JSON.parse(data);
            $(".modal-area").append(obj_modal["modal"]);
            $("#DetailDialog").Modal({"backdrop":true,"static":true});

          })
  })
}

function apply_job(announce_id) {
  var authtoken = localStorage.getItem('token');
  if(authtoken == null){
    Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.PleaseLogin,6000);
  }else {
    var api_url = getApi('applicant/apply_job');
    $.ajax({
      url: api_url,
      type: 'POST',
      headers:{"token":authtoken},
      data:{"announce_id":announce_id},
    })
    .done(function(data) {
      $(".closemodal").trigger("click")
      var obj = JSON.parse(data);
      if (obj.status==200) {
        Materialize.toast("<span class='success fas fa-check-circle'></span> &nbsp;"+lang.error.success,6000);
      }else if (obj.status==202) {
        Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.duplicate,6000);
      }
      else {
        Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.warning,6000);
      }
    })
  }

}

function favorite_job(announce_id) {
  var authtoken = localStorage.getItem('token');
  if(authtoken == null){
    Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.PleaseLogin,6000);
  }else {
    var api_url = getApi('applicant/favorite_job');
    $.ajax({
      url: api_url,
      type: 'POST',
      headers:{"token":authtoken},
      data:{"announce_id":announce_id},
    })
    .done(function(data) {
      var obj = JSON.parse(data);
      if (obj.status==200) {
        Materialize.toast("<span class='success fas fa-check-circle'></span> &nbsp;"+lang.error.success,6000);
      }else if (obj.status==202) {
        Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.duplicate,6000);
      }else {
        Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.warning,6000);
      }
    })
  }

}

// function close_detail() {
//   $(".announce_detail").css({"display":"none"})
//   $(".announce").css({"display":""})
//   $("#announce_id").val('')
//   $("#company_name").html('');
//   $("#logo_company").removeAttr("src");
//   $("#company_kind").html('');
//   $("#location").html('');
//   $("#job_description_1").html('');
//   $("#announce_title").html('');
//
//   $("#type_job").html('');
//   $("#income").html('');
//   $("#gender").html('');
//   $("#age").html('');
//   $("#education").html('');
//   $("#license").html('');
//   $("#working_day").html('');
//   $("#working_start").html('');
//   $("#working_end").html('');
//   $("#property").html('');
//   $("#working_start_date").html('');
//
//   $("#benefits").html('');
//
//   $("#fullname").html('');
//   $("#email_contact").html('');
//   $("#tel").html('');
//
//   $("#company_detail").html('');
//   $("#company_address").html('');
//   $("#map").removeAttr("src");
//
//
// }
